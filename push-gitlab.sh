#!/bin/sh

DATE=`date +"%Y-%m-%d %H:%M"`
TESTO="modifiche automatiche del $DATE"
TESTOGIT=${1:-$TESTO}


git add . 
git commit -m "$TESTOGIT"
git push
