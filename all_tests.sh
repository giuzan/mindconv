#!/usr/bin/bash

# create out dir if not exists
out_dir=./test/output


mkdir -p $out_dir
#rm -f out_no_bk/*


# 0.test GV files

for file in ./test/*.gv; 
	do 
	basefile=$(basename $file)
	echo "./mindconv.py $file -o $out_dir/$basefile"
	./mindconv.py $file -o $out_dir/$basefile $1
done

# 1.test CSV files

for file in ./test/*.csv; 
	do 
	basefile=$(basename $file)
	echo "./mindconv.py $file -o $out_dir/$basefile"
	./mindconv.py $file -o $out_dir/$basefile $1
done


# 2.test ADOC files

for file in ./test/*.adoc; 
	do 
	basefile=$(basename $file)
	echo "./mindconv.py $file -o $out_dir/$basefile"
	./mindconv.py $file -o $out_dir/$basefile $1
done


# 3.test VUE files

for file in ./test/*.vue; 
	do 
	basefile=$(basename $file)
	echo "./mindconv.py $file -o $out_dir/$basefile"
	./mindconv.py $file -o $out_dir/$basefile $1
done

# 4. Test graphml

# 5. test gml

# 6. test mm

# 7. test txt file con tab
for file in ./test/*.txt; 
	do 
	basefile=$(basename $file)
	echo "./mindconv.py $file -o $out_dir/$basefile"
	./mindconv.py $file -o $out_dir/$basefile $1
done

# 8. test docbook files
for file in ./test/*.docbook; 
	do 
	basefile=$(basename $file)
	echo "./mindconv.py $file -o $out_dir/$basefile"
	./mindconv.py $file -o $out_dir/$basefile $1
done
