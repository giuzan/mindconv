# code adapted from dot2vue.py Copyright (c) 2009-2012, Yan Fang, D. Stott Parker

import colorsys
import mcv_colors
import mcv_data
import os

import json



def adoc2docbook(input):
	"""convert an adoc file to docbook format """
	import subprocess

	adoc_cmd = '/usr/bin/asciidoctor'

	output_bytes = subprocess.check_output([adoc_cmd, '-b','docbook','-o','-',input])
	output = str(output_bytes,'utf-8')
	return output

def NOT_USED_docbook2list(docbook_xml):
	"""
	convert from docbook to list
	"""
	from bs4 import BeautifulSoup
	soup = BeautifulSoup(docbook_xml,"xml")
	
	for tag in soup.find_all():
		print(tag.name)
	
	out = []
	
	#TODO
	
	return out
	

def NOT_USED_adoc_to_list(input):
	"""
	read an adoc file and return a list with each raw a text bloc (paragraph, title, .... ) 
	"""
	out = []
	new_paragraph = False
	blocktype= ''
	label = ''
	arch_label = ''
	
	with open(input) as f:
		for line in f:
			if new_paragraph and blocktype:
				out.append({'blocktype':blocktype,'label':label,'arch_label':arch_label})
				blocktype= ''
				label = ''
				new_paragraph = False
				arch_label = ''
			
			# =================================  title
			if line.startswith('='):
				# get ID based 
				tuple_label = line.partition(' ')
				label = tuple_label[2].strip()
				id_level = tuple_label[0].count('=')
				out.append({'blocktype':'title','id_level':id_level,'label':label,'arch_label':arch_label})
				blocktype= ''
				arch_label = ''
			
			################################### point (edgle label)
			elif line.startswith('.'):
				arch_label = line[1:].strip()
				#out.append({'blocktype':'edge','label':label})
				blocktype= ''
				
			################################### commento 
			elif line.startswith('//'):
				comment = line   # non faccio nulla con comment
			
			#  ================================= Unordered list
			elif line.startswith('*') or line.startswith('-'):
				new_paragraph = True
				blocktype = 'ulist'
				
				#label
				tuple_label = line.partition(' ')
				label = tuple_label[2].strip()
				
			################################### empty line
			elif not line.strip():
				new_paragraph = True
			
			##### all the rest (e.g. text line)
			else: 
				if new_paragraph:
					new_paragraph = False
					label = line.strip()
					blocktype = 'text'
				else:
					label = label+"\n"+ line.strip()
		
	# add last line if needed
	if blocktype:
		out.append({'blocktype':blocktype,'label':label,'arch_label':arch_label})
	return out
				

def convert_shape(in_shape,output_format):
	shape_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),'shapes.json')
	with open(shape_path) as json_file:  
		mcv_shapes = json.load(json_file)
	try:
		shape = mcv_shapes[in_shape.lower()][output_format]
	except:
		logger.info("Shape:'{}' not found for format '{}'".format(in_shape , output_format))
		shape = mcv_shapes['roundRect'][output_format]
		
	return shape


def chunks(s, cl):
	"""Split a string or sequence into pieces of length cl and return an iterator
	"""
	for i in range(0, len(s), cl):
		yield s[i:i+cl]


#------------------------------------------------------------------------------------------------
#   Color conversion
#------------------------------------------------------------------------------------------------



def convert_color(color):
	
	color = color.strip()
	
	"""Convert color to a format usable by VUE"""
	# Graphviz uses the following color formats:
	#	"#%2x%2x%2x"	 Red-Green-Blue (RGB)
	#	"#%2x%2x%2x%2x" Red-Green-Blue-Alpha (RGBA)
	#	H[, ]+S[, ]+V	Hue-Saturation-Value (HSV) 0.0 <= H,S,V <= 1.0
	#	stringcolor name

	if color.find(':') >= 0:	#this 'color' is actually multiple colors separated by ':'
		colors = color.split(':')
		return ':'.join([convert_color(c) for c in colors])

	if color.find(',') >= 0 and color.find(' ') >= 0: color = color.replace(' ','')
	if color.startswith('#'):
		# --- format: RGB or RBGA
		t = list(chunks(color[1:],2))
		if len(t) > 3: t = t[0:3]
		colstr = '#' + ''.join(t)
		#print colstr
		return colstr
	elif (len(color.split(' '))==3):
		# --- format: blank-delimited HSV
		hsv = color.split(' ')
		colstr = hsv_rgb(hsv)
		#print colstr
		return colstr
	elif (len(color.split(','))==3):
		# --- format: comma-delimited HSV
		hsv = color.split(',')
		colstr = hsv_rgb(hsv)
		#print colstr
		return colstr
	else:
		# --- format: a color name
		return graphviz_colorname_to_hex(color, 'x11')

def hsv_rgb(hsv):
	try:
		H = color_percentify(hsv[0])
		S = color_percentify(hsv[1])
		V = color_percentify(hsv[2])
		R, G, B = colorsys.hsv_to_rgb(H, S, V)
		rgb = ( color_hexify(R), color_hexify(G), color_hexify(B) )
		rgbstr = "#" + ''.join( rgb )
	except:
		rgbstr = '#000000'
		
	return rgbstr

def color_percentify(x):
	try:
		p = float(x)
	except:
		p = 0
	return p

def color_hexify(p):
	try:
		x = '%02x' % round(p * 255.0)
	except:
		x = '00'
	return x

def graphviz_colorname_to_hex(ColorName, color_scheme):
	 colorname = ColorName.lower()
	 colorname = colorname.replace('grey','gray')
	 colorname = colorname.replace('_','')
	 return mcv_colors.color_scheme[color_scheme][colorname]	
