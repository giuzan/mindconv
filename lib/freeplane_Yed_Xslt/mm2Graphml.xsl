<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:math="java:java.lang.Math"
	xmlns:g="http://graphml.graphdrawing.org/xmlns/graphml"
	exclude-result-prefixes="math g" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns/graphml"

	xmlns:graphml="http://graphml.graphdrawing.org/xmlns" xmlns:y="http://www.yworks.com/xml/graphml"
	xmlns:yed="http://www.yworks.com/xml/yed/3">

	<xsl:output method="xml" indent="yes" encoding="UTF-8"
		standalone="no" />
	<xsl:namespace-alias stylesheet-prefix="g"
		result-prefix="#default" />

	<xsl:template match="/map">
		<graphml xmlns="http://graphml.graphdrawing.org/xmlns"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:y="http://www.yworks.com/xml/graphml"
			xmlns:yed="http://www.yworks.com/xml/yed/3"
			xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd">
			<!--Created by yFiles for Java 2.9 -->
			<key for="graphml" id="d0" yfiles.type="resources" />
			<key for="port" id="d1" yfiles.type="portgraphics" />
			<key for="port" id="d2" yfiles.type="portgeometry" />
			<key for="port" id="d3" yfiles.type="portuserdata" />
			<key attr.name="url" attr.type="string" for="node" id="d4" />
			<key attr.name="description" attr.type="string" for="node" id="d5" />
			<key for="node" id="d6" yfiles.type="nodegraphics" />
			<key attr.name="Beschreibung" attr.type="string" for="graph" id="d7" />
			<key attr.name="url" attr.type="string" for="edge" id="d8" />
			<key attr.name="description" attr.type="string" for="edge" id="d9" />
			<key for="edge" id="d10" yfiles.type="edgegraphics" />
			<xsl:apply-templates select="node[node]">
			</xsl:apply-templates>
			<xsl:call-template name="edges" />
		</graphml>
	</xsl:template>

	<!-- Template for a groupNode -->
	<xsl:template match="node[node]">
		<xsl:param name="index" select="0" />
		<xsl:param name="colo" select="'#FFCC00'" />
		<graph edgedefault="directed" id="{@ID}">
			<data key="d7" />
			<node yfiles.foldertype="group">
				<xsl:attribute name="id"><xsl:value-of select="@ID" /></xsl:attribute>
				<data key="d4">
					<xsl:value-of select="@LINK" />
				</data>
				<data key="d5">
					<xsl:copy-of select="string(richcontent[@TYPE='NOTE']/html/body)" />
				</data>
				<data key="d6">
					<y:ProxyAutoBoundsNode>
						<y:Realizers active="0">
							<y:GroupNode>
								<y:Geometry height="414.1297619047619" width="791.0654296875"
									x="0.83349609375" y="0.0" />
								<y:Fill color="#CAECFF84" transparent="false" />
								<y:BorderStyle color="#666699" type="dotted"
									width="1.0" />
								<y:NodeLabel alignment="right" autoSizePolicy="node_width"
									borderDistance="0.0" fontFamily="Dialog" fontSize="15"
									fontStyle="plain" hasLineColor="false" height="22.37646484375"
									modelName="internal" modelPosition="t" textColor="#000000"
									visible="true" width="791.0654296875" x="0.0" y="0.0">
									<xsl:choose>
										<xsl:when test="edge/@COLOR != ''">
											<xsl:attribute name="backgroundColor"><xsl:value-of
												select="edge/@COLOR" /></xsl:attribute>
											<!-- Assign mother color to temp_coll -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="backgroundColor"><xsl:value-of
												select="$colo" /></xsl:attribute>
											<!-- Assign enherited value to temp_coll -->
										</xsl:otherwise>
									</xsl:choose>
									<xsl:value-of select="@TEXT" />
									<xsl:copy-of select="string(richcontent[@TYPE='DETAILS']/html/body)" />
								</y:NodeLabel>
								<y:Shape type="roundrectangle" />
								<y:State closed="false" innerGraphDisplayEnabled="false" />
								<y:Insets bottom="15" bottomF="15.0" left="15" leftF="15.0"
									right="15" rightF="15.0" top="15" topF="15.0" />
								<y:BorderInsets bottom="1" bottomF="1.0" left="25"
									leftF="25.0" right="25" rightF="25.0" top="1" topF="1.0001224578373353" />
							</y:GroupNode>
							<y:GroupNode>
								<y:Geometry height="80.0" width="100.0" x="-50.0" y="-30.0" />
								<y:Fill color="#CAECFF84" transparent="false" />
								<y:BorderStyle color="#666699" type="dotted"
									width="1.0" />
								<y:NodeLabel alignment="right" autoSizePolicy="node_width"
									borderDistance="0.0" fontFamily="Dialog" fontSize="15"
									fontStyle="plain" hasLineColor="false" height="22.37646484375"
									modelName="internal" modelPosition="t" textColor="#000000"
									visible="true" width="791.0654296875" x="0.0" y="0.0">
									<xsl:choose>
										<xsl:when test="edge/@COLOR != ''">
											<xsl:attribute name="backgroundColor"><xsl:value-of
												select="edge/@COLOR" /></xsl:attribute>
											<!-- Assign mother color to temp_coll -->
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="backgroundColor"><xsl:value-of
												select="$colo" /></xsl:attribute>
											<!-- Assign enherited value to temp_coll -->
										</xsl:otherwise>
									</xsl:choose>
									<xsl:value-of select="@TEXT" />
								</y:NodeLabel>
								<y:Shape type="roundrectangle" />
								<y:State closed="true" innerGraphDisplayEnabled="false" />
								<y:Insets bottom="15" bottomF="15.0" left="15" leftF="15.0"
									right="15" rightF="15.0" top="15" topF="15.0" />
								<y:BorderInsets bottom="0" bottomF="0.0" left="0"
									leftF="0.0" right="0" rightF="0.0" top="0" topF="0.0" />
							</y:GroupNode>
						</y:Realizers>
					</y:ProxyAutoBoundsNode>
				</data>
				<xsl:apply-templates select="node[node]">
					<xsl:with-param name="index" select="$index + 1" />
					<xsl:with-param name="colo">
						<xsl:value-of select="$colo" />
					</xsl:with-param>
				</xsl:apply-templates>
				<graph edgedefault="directed" id="{@ID}">
					<xsl:apply-templates select="node[not(node)]">
						<xsl:with-param name="index" select="$index + 1" />
						<xsl:with-param name="colo">
							<xsl:value-of select="$colo" />
						</xsl:with-param>
					</xsl:apply-templates>
				</graph>
			</node>
		</graph>
	</xsl:template>

	<!-- Template for a simple y:node -->
	<xsl:template match="node[not(node)]">
		<xsl:param name="index" />
		<xsl:param name="colo" select="'#FFCC00'" />
		<node>
			<xsl:attribute name="id"><xsl:value-of select="@ID" /></xsl:attribute>
			<data key="d4">
				<xsl:value-of select="@LINK" />
			</data>
			<data key="d5">
				<xsl:copy-of select="string(richcontent[@TYPE='NOTE']/html/body)" />
			</data>
			<data key="d6">
				<y:GenericNode configuration="com.yworks.entityRelationship.big_entity">
					<y:Geometry height="120.0" width="80.0" x="1236.0" y="211.5" />
					<y:Fill color2="#B7C9E3" transparent="false">
						<xsl:choose>
							<xsl:when test="edge/@COLOR != ''">
								<xsl:attribute name="color"><xsl:value-of
									select="edge/@COLOR" /></xsl:attribute>
								<!-- Assign mother color to temp_coll -->
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="color"><xsl:value-of
									select="$colo" /></xsl:attribute>
								<!-- Assign enherited value to temp_coll -->
							</xsl:otherwise>
						</xsl:choose>
					</y:Fill>
					<y:BorderStyle color="#000000" type="line" width="1.0" />
					<y:NodeLabel alignment="center" autoSizePolicy="content"
						backgroundColor="#B7C9E3" configuration="CroppingLabel"
						fontFamily="Dialog" fontSize="12" fontStyle="plain" hasLineColor="false"
						height="22.09375" modelName="internal" modelPosition="t"
						textColor="#000000" underlinedText="true" visible="true" width="37.01171875"
						x="21.494140625" y="4.0">
						<xsl:value-of select="@TEXT" />
					</y:NodeLabel>
					<y:NodeLabel alignment="left" autoSizePolicy="content"
						configuration="com.yworks.entityRelationship.label.attributes"
						fontFamily="Dialog" fontSize="12" fontStyle="plain"
						hasBackgroundColor="false" hasLineColor="false" height="62.8046875"
						modelName="custom" textColor="#000000" visible="true" width="57.3671875"
						x="2.0" y="34.09375">
						<xsl:copy-of select="string(richcontent[@TYPE='DETAILS']/html/body)" />
						<y:LabelModel>
							<y:ErdAttributesNodeLabelModel />
						</y:LabelModel>
						<y:ModelParameter>
							<y:ErdAttributesNodeLabelModelParameter />
						</y:ModelParameter>
					</y:NodeLabel>
					<y:StyleProperties>
						<y:Property class="java.lang.Boolean"
							name="y.view.ShadowNodePainter.SHADOW_PAINTING" value="true" />
					</y:StyleProperties>
				</y:GenericNode>
			</data>
		</node>
		<xsl:apply-templates select="node[not(node)]">
			<xsl:with-param name="index" select="0" />
			<xsl:with-param name="colo">
				<xsl:value-of select="$colo" />
			</xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>


	<!-- Template for a edges -->
	<xsl:template name="edges">
		<xsl:for-each select="//arrowlink">
			<edge>
				<xsl:attribute name="id"><xsl:value-of select="../@ID" />::<xsl:value-of
					select="@DESTINATION" />::<xsl:number format="1" /><xsl:value-of
					select="." /><xsl:text /></xsl:attribute>
				<xsl:attribute name="source"><xsl:value-of select="../@ID" /></xsl:attribute>
				<xsl:attribute name="target"><xsl:value-of select="@DESTINATION" /></xsl:attribute>
				<data key="d10">
					<y:PolyLineEdge>
						<y:LineStyle type="line" width="1.0">
							<xsl:attribute name="color"><xsl:value-of
								select="@COLOR" /></xsl:attribute>
							<xsl:attribute name="width"><xsl:value-of
								select="@WIDTH" /></xsl:attribute>
						</y:LineStyle>
						<y:Arrows source="none" target="standard" />
						<y:EdgeLabel visible="true">
							<xsl:value-of select="@MIDDLE_LABEL"></xsl:value-of>
						</y:EdgeLabel>
						<y:BendStyle smoothed="false" />
					</y:PolyLineEdge>
				</data>
			</edge>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="output-note-text-as-details">
		<xsl:if test="richcontent[@TYPE='DETAILS']">
			<Detail>
				<xsl:copy-of select="string(richcontent[@TYPE='DETAILS']/html/body)" />
			</Detail>
			<DetailHTML>
				<xsl:copy-of select="richcontent[@TYPE='DETAILS']/html/body/*" />
			</DetailHTML>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
  