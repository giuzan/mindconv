# manage vue format
# 20180324

import random
import re
import mcv_lib
import networkx as nx

def networkx2vue(G,outfile):
	"""
	convert the networkx graph to vue format
	TODO: use beautiful soup instead of sting concatenation
	"""
	
	# convert color to a vue format
	
	out = vue_xml_head()

	# analisys of all nodes
	for n in G:
		out += vue_write_node(n,G.nodes[n])

	# analisys of all edges 
	# Start ID of edges 20 + number of nodes (ID of nodes start from 10) 
	id =  nx.number_of_nodes(G) + 20
	for (e_from,e_to,e_data) in G.edges(data=True):
		out += vue_write_link(id,e_from,e_to,e_data)
		id += 1
	
	out += vue_xml_tail()
	
	f = open(outfile+'.vue','w')
	f.write(out)


def vue_xml_head():
	out = """<!-- Tufts VUE 3.3.0 concept-map (empty.vue) 2017-03-22 -->
<!-- Tufts VUE: http://vue.tufts.edu/ -->
<!-- Do Not Remove: VUE mapping @version(1.1) jar:file:/opt/vue/VUE.jar!/tufts/vue/resources/lw_mapping_1_1.xml -->
<!-- Do Not Remove: Saved date Wed Mar 22 16:52:17 CET 2017 by raf on platform Linux 4.10.2-1-ARCH in JVM 1.8.0_121-b13 -->
<!-- Do Not Remove: Saving version @(#)VUE: built October 8 2015 at 1658 by tomadm on Linux 2.6.32-504.23.4.el6.x86_64 i386 JVM 1.7.0_21-b11(bits=32) -->
<?xml version="1.0" encoding="US-ASCII"?>
<LW-MAP xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:noNamespaceSchemaLocation="none" ID="0" label="empty.vue"
    created="1490197928677" x="0.0" y="0.0" width="1.4E-45"
    height="1.4E-45" strokeWidth="0.0" autoSized="false">
    <resource referenceCreated="1490197937700"
        spec="empty.vue"
        type="1" xsi:type="URLResource">
        <title>empty.vue</title>
        <property key="File" value="empty.vue"/>
    </resource>
    <fillColor>#FFFFFF</fillColor>
    <strokeColor>#404040</strokeColor>
    <textColor>#000000</textColor>
    <font>SansSerif-plain-14</font>
    <URIString>http://vue.tufts.edu/rdf/resource/f6b7fe260a861f20456c38d38f4f07be</URIString>
    <layer ID="1" label="Layer 1" created="1490197928682" x="0.0"
        y="0.0" width="1.4E-45" height="1.4E-45" strokeWidth="0.0" autoSized="false">
        <URIString>http://vue.tufts.edu/rdf/resource/f6b7fe2b0a861f20456c38d34cc8dff0</URIString>
    </layer>
	"""
	return out


	
def vue_write_node(ID,node):
	VUE_round_rect = """<shape arcwidth="20.0" archeight="20.0" xsi:type="roundRect"/>"""
	VUE_shape = """<shape xsi:type="%s"/>"""
		
	out = """<child ID="###ID###" label="###LABEL###" layerID="1" created="0" x="###X###"
        y="###Y###" width="###WIDTH###" height="###HEIGHT###" strokeWidth="1.0"
        autoSized="false" xsi:type="node">
		<fillColor>###FILLCOLOR###</fillColor>
        <strokeColor>###STROKECOLOR###</strokeColor>
        <textColor>###TEXTCOLOR###</textColor>
        <font>###FONT###</font>
       ###SHAPE###
    </child>
	"""
		
	try:
		x=node['mc']['x']
		y=node['mc']['y']
	except:
		x = random.randint(0,999)
		y = random.randint(0,999)
		
	try:
		w = node['mc']['width']
		h = node['mc']['height']
	except: 
		w = config['vue']['NODE_WIDTH']
		h = config['vue']['NODE_HEIGHT']
	

	# transofrm label with newline
	try:
		label = re.sub("\n","&#xa;",node['label'])
	except: 
		print(node,"NOLABEL")
		label = ''
	
	out = re.sub("###ID###", str(ID), out)
	out = re.sub("###LABEL###", label, out)
	out = re.sub("###X###", str(x), out)
	out = re.sub("###Y###", str(y), out)
	out = re.sub("###WIDTH###", str(w), out)
	out = re.sub("###HEIGHT###", str(h), out)
	
	try:
		out = re.sub("###FILLCOLOR###", mcv_lib.convert_color(node['mc']['fillcolor']), out)
	except:
		out = re.sub("###FILLCOLOR###","#FCDBD9", out)
	
	try:
		out = re.sub("###STROKECOLOR###", mcv_lib.convert_color(node['mc']['strokecolor']), out)
	except:
		out = re.sub("###STROKECOLOR###","#000001", out)
		
	try:
		out = re.sub("###TEXTCOLOR###", mcv_lib.convert_color(node['mc']['textcolor']), out)
	except:
		out = re.sub("###TEXTCOLOR###","#000002", out)
		
		
	if (node['mc'].get('font') != None):
		out = re.sub("###FONT###", node['mc']['font'], out)
	else:
		out = re.sub("###FONT###",config['vue']['NODE_FONT'], out)
		
	if (node['mc'].get('shape') == None):		
		out = re.sub("###SHAPE###",VUE_round_rect, out)
	else:
		shape = mcv_lib.convert_shape(node['mc']['shape'],'vue')
		if shape == 'roundRect':
			out = re.sub("###SHAPE###",VUE_round_rect, out)
		else:
			out = re.sub("###SHAPE###",VUE_shape % shape, out)
		
	return out

def vue_write_link(id,e_from,e_to,link):
	
	out = """<child ###ID### ###LABEL### layerID="1" created="0"  strokeWidth="1.0"
        autoSized="false" controlCount="0" arrowState="2" xsi:type="link">
        <strokeColor>#404040</strokeColor>
        <textColor>#404040</textColor>
        <font>Arial-plain-11</font>
        <ID1 xsi:type="node">###FROM###</ID1>
        <ID2 xsi:type="node">###TO###</ID2>
    </child>
	"""

	out = re.sub("###FROM###", str(e_from), out)
	out = re.sub("###TO###", str(e_to), out)
	
	try:
		# transofrm label 
		label = re.sub("\n","&#xa;",link['label'])
		#label = str(label.encode('ascii', 'xmlcharrefreplace'))	
		out = re.sub("###LABEL###", 'label="'+label+'"', out)
	except:
		out = re.sub("###LABEL###", '', out)
	
	
	try:
		out = re.sub("###ID###", 'ID="'+str(id)+'"', out)
	except:
		out = re.sub("###ID###", '', out)
	
	
	return out


	
def vue_xml_tail():
	out = """<userZoom>1.0</userZoom>
    <userOrigin x="-14.0" y="-14.0"/>
    <presentationBackground>#202020</presentationBackground>
    <PathwayList currentPathway="0" revealerIndex="-1">
        <pathway ID="0" label="Percorso senza titolo"
            created="1490197928676" x="0.0" y="0.0" width="1.4E-45"
            height="1.4E-45" strokeWidth="0.0" autoSized="false"
            currentIndex="-1" open="true">
            <strokeColor>#B3CC33CC</strokeColor>
            <textColor>#000000</textColor>
            <font>SansSerif-plain-14</font>
            <URIString>http://vue.tufts.edu/rdf/resource/f6b7fe2d0a861f20456c38d31fc423cf</URIString>
            <masterSlide ID="2" created="1490197928787" x="0.0" y="0.0"
                width="800.0" height="600.0" locked="true"
                strokeWidth="0.0" autoSized="false">
                <fillColor>#000000</fillColor>
                <strokeColor>#404040</strokeColor>
                <textColor>#000000</textColor>
                <font>SansSerif-plain-14</font>
                <URIString>http://vue.tufts.edu/rdf/resource/f6b7fe340a861f20456c38d38ddcda80</URIString>
                <titleStyle ID="3" label="Header"
                    created="1490197928844" x="329.0" y="174.5"
                    width="142.0" height="51.0" strokeWidth="0.0"
                    autoSized="true" isStyle="true" xsi:type="node">
                    <strokeColor>#404040</strokeColor>
                    <textColor>#FFFFFF</textColor>
                    <font>Gill Sans-plain-36</font>
                    <URIString>http://vue.tufts.edu/rdf/resource/f6b7fe350a861f20456c38d3ebee4c00</URIString>
                    <shape xsi:type="rectangle"/>
                </titleStyle>
                <textStyle ID="4" label="Slide Text"
                    created="1490197928845" x="339.5" y="282.5"
                    width="121.0" height="35.0" strokeWidth="0.0"
                    autoSized="true" isStyle="true" xsi:type="node">
                    <strokeColor>#404040</strokeColor>
                    <textColor>#FFFFFF</textColor>
                    <font>Gill Sans-plain-22</font>
                    <URIString>http://vue.tufts.edu/rdf/resource/f6b7fe360a861f20456c38d3fcc2a0a5</URIString>
                    <shape xsi:type="rectangle"/>
                </textStyle>
                <linkStyle ID="5" label="Links" created="1490197928850"
                    x="372.0" y="385.0" width="56.0" height="30.0"
                    strokeWidth="0.0" autoSized="true" isStyle="true" xsi:type="node">
                    <strokeColor>#404040</strokeColor>
                    <textColor>#B3BFE3</textColor>
                    <font>Gill Sans-plain-18</font>
                    <URIString>http://vue.tufts.edu/rdf/resource/f6b7fe370a861f20456c38d32e15d849</URIString>
                    <shape xsi:type="rectangle"/>
                </linkStyle>
            </masterSlide>
        </pathway>
    </PathwayList>
    <date>2017-03-22</date>
    <modelVersion>6</modelVersion>
    <saveLocation></saveLocation>
    <saveFile>empty.vue</saveFile>
</LW-MAP>"""
	return out

