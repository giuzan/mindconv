

# Graphviz shapes
# adapted directly from http://www.graphviz.org/doc/info/shapes.html#polygon

"""
mcv_shape = {
	'roundRect' : {'vue':'roundRect', 'dot':'box'},
	'box' 		: {'vue':'roundRect', 'dot':'box'},
	'diamond'	: {'vue':'diamond', 'dot':'diamond'},
	}

with open('lib/shape.json', 'w') as outfile:  
    json.dump(mcv_shape, outfile, indent=4)
"""

vue_equivalent = {
   'box' :                 'rectangle',
   'polygon' :             'rhombus',
   'ellipse' :             'ellipse',
   'oval' :                'ellipse',
   'circle' :              'ellipse',
   'point' :               'ellipse',
   'egg' :                 'ellipse',
   'triangle' :            'triangle',
   'plaintext' :           'rectangle',
   'diamond' :             'diamond',
   'trapezium' :           'triangle',
   'parallelogram' :       'rhombus',
   'house' :               'triangle',
   'pentagon' :            'hexagon',
   'hexagon' :             'hexagon',
   'septagon' :            'octagon',
   'octagon' :             'octagon',
   'doublecircle' :        'ellipse',
   'doubleoctagon' :       'octagon',
   'tripleoctagon' :       'octagon',
   'invtriangle' :         'shield',
   'invtrapezium' :        'shield',
   'invhouse' :            'shield',
   'Mdiamond' :            'diamond',
   'Msquare' :             'rectangle',
   'Mcircle' :             'ellipse',
   'rect' :                'rectangle',
   'rectangle' :           'rectangle',
   'square' :              'rectangle',
   'none' :                'rectangle',
   'note' :                'rectangle',
   'tab' :                 'rectangle',
   'folder' :              'rectangle',
   'box3d' :               'rectangle',
   'component' :           'rectangle'
}
