# manage graphml format
# 20230831

import random
import re
import mcv_lib
import os


def networkx2graphml(G,outfile):
	
	H = G.copy()
	
	for n in H:
		# Remove internal mindconv 'mc' attributes	
		try: 
			del H.nodes[n]['mc']
		except:
			print("ERROR : on write_graphml - del H.nodes[n]['mc']")
			
	try: 
		nx.write_graphml(H, outfile+'.graphml', encoding='utf-8', prettyprint=True)
	except:
		print("ERROR : on write_graphml")
	
def networkx2graphml_yed(G,outfile):
	
	# export to graphml format with all features to be read by yed
	# https://www.yworks.com/products/yed
		
	# resources are the list of pictures to add
	resources = [] 	
	
	out_head = yed_graphml_head()
	out_tail = yed_graphml_tail()
	
	# analisys of all nodes
	node_type = 'main_node'
	for n in G:
		if node_type == 'main_node':
			node_type = 'standard'
			out_head += yed_write_node(n,G.nodes[n],'main_node')
		else:
			out_head += yed_write_node(n,G.nodes[n])
			
		try:
			image = G.nodes[n]['mc']['image']
		except:
			image = ''
			
		try:
			image_size=G.nodes[n]['mc']['img_size']
		except:
			image_size = ''
			
		if image != '':
			out_tail = yed_write_resource(out_tail,n,image,image_size)

			
	# analisys of all edges 
	id = 0
	for (e_from,e_to,e_data) in G.edges(data=True):
		out_head += yed_write_link(id,e_from,e_to,e_data,G.is_directed())
		id += 1
	
	out = out_head + out_tail
	
	#remove ###RESOURCES### tag from string
	out = re.sub('###RESOURCES###','', out)

	f = open(outfile+'-yed.graphml','w')
	f.write(out)

	
	return out

def yed_write_resource(out,n,image_url,image_size):
	import html
	import subprocess 
	import shlex
	

	
	if image_url.startswith('http'):
	
		import urllib.request 
		url_filename, url_extension = os.path.splitext(image_url)
		image_name = 'img_tmp'+url_extension
		try: 
			urllib.request.urlretrieve(image_url,image_name)
			image = image_name
		except Exception as error:
			# handle the exception
			print(f"An exception occurred: {error} on url {url_filename}") 
			
				
	else:
		image=image_url
	
	filename, file_extension = os.path.splitext(image)
	
	xml_resource_svg =  """<y:Resource id="###ID_RES###">
        <yed:ScaledIcon xScale="1.0" yScale="1.0">
          ###SVG###
        </yed:ScaledIcon>
      </y:Resource>
	  """
	
	xml_resource_img = """<y:Resource id="###ID_RES###" type="java.awt.image.BufferedImage">###IMG###</y:Resource>"""
	
	if file_extension.lower() == '.svg':
	
		if config['graphml']['IMG_RESIZE'].lower() in ['yes','true'] or image_size != '':
			tmp_svg=image+'_tmp.svg'	
			if image_size == '':
				width,height=config['graphml']['IMG_MAGICK_RESIZE'].split('x')
			else:
				width,height=image_size.split('x')
			
			#command_line = "inkscape -z --export-plain-svg={} -w {} -h {} {}".format(tmp_svg,width,height,image)
			command_line = "rsvg-convert -a -w {} -h {} -f svg {} -o {}".format(width,height,image,tmp_svg)
			a_cmd = shlex.split(command_line)
			subprocess.call(a_cmd)
			image = tmp_svg
	
		svg_str = open(image, 'r').read()
		svg_str_html_encoded = html.escape(svg_str)
		xml_resource_svg = re.sub("###ID_RES###",str(n),xml_resource_svg)
		xml_resource_svg = re.sub("###SVG###","<yed:SvgIcon>"+svg_str_html_encoded+"</yed:SvgIcon>",xml_resource_svg)
		out = re.sub("###RESOURCES###", xml_resource_svg+"###RESOURCES###", out)
		
	elif file_extension.lower() == '.jpg' or  file_extension.lower() == '.png':
		import base64
		
		with open(image, "rb") as image_file:
			encoded_string = base64.b64encode(image_file.read())
			encoded_string = encoded_string.decode("utf-8")
		
		img_base64=''	
		base64_lenght = 76
		for i in range((len(encoded_string)//base64_lenght)+1):
			img_base64 += encoded_string[i*base64_lenght:(i+1)*base64_lenght] + "&#13;\n"
			
		xml_resource_svg = re.sub("###ID_RES###",str(n),xml_resource_svg)
		xml_resource_svg = re.sub("###SVG###",'<yed:ImageIcon image="{}"/>'.format(str(n+100)),xml_resource_svg)
		xml_resource_img = re.sub("###ID_RES###",str(n+100),xml_resource_img)
		xml_resource_img = re.sub("###IMG###",img_base64,xml_resource_img)
		
		
		out = re.sub("###RESOURCES###", xml_resource_svg+xml_resource_img+"###RESOURCES###", out)
		
	"""
	elif file_extension.lower() == '.jpg' or  file_extension.lower() == '.png':
		# Convert the image in SVG so I can add it directly
		# without using java.awt.image.BufferedImage format
		
		magick_extra_cmd = ''
		
		if config['graphml']['IMG_RESIZE'].lower() in ['yes','true'] or image_size != '':
			if image_size == '':
				magick_extra_cmd += '-resize {} '.format(config['graphml']['IMG_MAGICK_RESIZE'])
			else:
				magick_extra_cmd += '-resize {} '.format(image_size)
		command_line = "convert {} {} {}.svg".format(image,magick_extra_cmd,image)
		print(command_line)
		a_cmd = shlex.split(command_line)
		subprocess.call(a_cmd)
		svg_str = open(image+'.svg', 'r').read()
		svg_str_html_encoded = cgi.escape(svg_str)
		xml_resource_svg = re.sub("###ID_RES###",str(n),xml_resource_svg)
		xml_resource_svg = re.sub("###SVG###",svg_str_html_encoded,xml_resource_svg)
		out = re.sub("###RESOURCES###", xml_resource_svg+"###RESOURCES###", out)		
		#os.remove(image+'.svg')
	"""
	# il created remove the image file
	# (except OSError:)
	try:
		os.remove(image_name)
	except:
		pass
	try:
		os.remove(tmp_svg)
	except:
		pass
		
	return out
def yed_write_node(ID,node,node_type='graphml'):
	# node_type is the format of the node
	# taken from config.ini
	
	out = """
	<node id="n###ID###">
      <data key="d5"/>
      <data key="d6">
        <y:ShapeNode>
          <y:Geometry height="###HEIGHT###" width="###WIDTH###" x="###X###" y="###Y###"/>
          <y:Fill ###FILLCOLOR### transparent="false"/>
          <y:BorderStyle color="###STROKECOLOR###" raised="false" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="###FONT_FAMILY###" fontSize="###FONT_SIZE###" fontStyle="###NODE_FONT_STYLE###" hasBackgroundColor="false" hasLineColor="false" height="17.96875" horizontalTextPosition="center" ###ICON_DATA### iconTextGap="4" modelName="custom" textColor="###TEXTCOLOR###" verticalTextPosition="bottom" visible="true" width="12.208984375" x="5.0" y="6.015625">###LABEL###<y:LabelModel>
              <y:SmartNodeLabelModel distance="4.0"/>
            </y:LabelModel>
            <y:ModelParameter>
              <y:SmartNodeLabelModelParameter labelRatioX="0.0" labelRatioY="0.0" nodeRatioX="0.0" nodeRatioY="0.0" offsetX="0.0" offsetY="0.0" upX="0.0" upY="-1.0"/>
            </y:ModelParameter>
          </y:NodeLabel>
          <y:Shape type="###SHAPE###"/>
        </y:ShapeNode>
      </data>
    </node>
	"""
	
	try:
		x=node['mc']['x']
		y=node['mc']['y']
	except:
		x = random.randint(0,999)
		y = random.randint(0,999)
		
	try:
		w = node['mc']['width']
		h = node['mc']['height']
	except: 
		w = config[node_type]['NODE_WIDTH']
		h = config[node_type]['NODE_HEIGHT']


	# transofrm label with newline
	try:
		label = re.sub("\n","&#xa;",node['label'])
	except: 
		label = ''
	
	out = re.sub("###ID###", str(ID), out)
	out = re.sub("###LABEL###", label, out)
	out = re.sub("###X###", str(x), out)
	out = re.sub("###Y###", str(y), out)
	out = re.sub("###WIDTH###", str(w), out)
	out = re.sub("###HEIGHT###", str(h), out)


	# FILL COLOR : background color of the node
	try:
		fill_color = 'color="{}"'.format(mcv_lib.convert_color(node['mc']['fillcolor']))
	except:
		if config[node_type]['NODE_FILLCOLOR'] == 'transparent':
			fill_color = 'hasColor="false"'
		else:
			fill_color = 'color="{}"'.format(mcv_lib.convert_color(config[node_type]['NODE_FILLCOLOR']))
		
	out = re.sub("###FILLCOLOR###",fill_color , out)
	

	# STROKECOLOR 
	try:
		stroke_color = mcv_lib.convert_color(node['mc']['strokecolor'])
	except:
		stroke_color = mcv_lib.convert_color(config[node_type]['NODE_STROKECOLOR'])
	out = re.sub("###STROKECOLOR###", stroke_color, out)

	# TEXTCOLOR
	try:
		text_color = mcv_lib.convert_color(node['mc']['textcolor'])
	except:
		text_color = mcv_lib.convert_color(config[node_type]['NODE_TEXTCOLOR'])
	out = re.sub("###TEXTCOLOR###",text_color, out)
	
	# SHAPE
	if (node['mc'].get('shape') == None):		
		shape = mcv_lib.convert_shape(config[node_type]['NODE_SHAPE'],'yed')
	else:
		shape = mcv_lib.convert_shape(node['mc']['shape'],'yed')
	out = re.sub("###SHAPE###",shape, out)
		
	# FONT_SIZE 
	try:
		stroke_color = node['mc']['font_size']
	except:
		stroke_color = config[node_type]['NODE_FONT_SIZE']
	out = re.sub("###FONT_SIZE###", stroke_color, out)

	###FONT_FAMILY###
	try:
		stroke_color = node['mc']['font_family']
	except:
		stroke_color = config[node_type]['NODE_FONT_FAMILY']
	out = re.sub("###FONT_FAMILY###", stroke_color, out)

	###NODE_FONT_STYLE###
	try:
		stroke_color = node['mc']['font_style']
	except:
		stroke_color = config[node_type]['NODE_FONT_STYLE']
	out = re.sub("###NODE_FONT_STYLE###", stroke_color, out)	
	
	# IMAGE	
	if (node['mc'].get('image') == None):		
		 image = '' 
		 out = re.sub("###ICON_DATA###",'', out)
	else:
		 out = re.sub("###ICON_DATA###",'iconData="{}"'.format(ID), out)
	
	return out
	
def yed_write_link(id,e_from,e_to,link,directed=False):
	
	out = """ <edge id="e###ID###" source="n###FROM###" target="n###TO###">
      <data key="d9"/>
      <data key="d10">
        <y:PolyLineEdge>
          <y:Path sx="0.0" sy="0.0" tx="0.0" ty="0.0"/>
          <y:LineStyle color="#000000" type="line" width="1.0"/>
          <y:Arrows source="none" target="###ARROW_TARGET###"/>
		  ###OPTIONALLABEL###
          <y:BendStyle smoothed="false"/>
        </y:PolyLineEdge>
      </data>
    </edge>
	"""
	
	label_text="""<y:EdgeLabel alignment="center" backgroundColor="#FFFFFF" configuration="AutoFlippingLabel" distance="2.0" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasLineColor="false" height="17.96875" horizontalTextPosition="center" iconTextGap="4" modelName="centered" modelPosition="center" preferredPlacement="anywhere" ratio="0.5" textColor="#000000" verticalTextPosition="bottom" visible="true" width="69.384765625" x="-30.501907348632812" y="39.015625">###LABEL###<y:PreferredPlacementDescriptor angle="0.0" angleOffsetOnRightSide="0" angleReference="absolute" angleRotationOnRightSide="co" distance="-1.0" frozen="true" placement="anywhere" side="anywhere" sideReference="relative_to_edge_flow"/>
          </y:EdgeLabel>"""

	out = re.sub("###FROM###", str(e_from), out)
	out = re.sub("###TO###", str(e_to), out)
	out = re.sub("###ID###", str(id), out)
	
	try:
		label = link['label'].strip()
	except:
		label = ''
		
	if label != '' :
		label_text = re.sub("###LABEL###", label, label_text)
		out = re.sub("###OPTIONALLABEL###",label_text, out)
	else:
		out = re.sub("###OPTIONALLABEL###", '', out)
	
	if directed:
		out = re.sub("###ARROW_TARGET###", 'standard', out)
	else:
		out = re.sub("###ARROW_TARGET###", 'none', out)
	
	return out 


def yed_graphml_head():
	out="""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:java="http://www.yworks.com/xml/yfiles-common/1.0/java" xmlns:sys="http://www.yworks.com/xml/yfiles-common/markup/primitives/2.0" xmlns:x="http://www.yworks.com/xml/yfiles-common/markup/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:y="http://www.yworks.com/xml/graphml" xmlns:yed="http://www.yworks.com/xml/yed/3" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd">
  <!--Created by mindconv-->
  <key attr.name="Description" attr.type="string" for="graph" id="d0"/>
  <key for="port" id="d1" yfiles.type="portgraphics"/>
  <key for="port" id="d2" yfiles.type="portgeometry"/>
  <key for="port" id="d3" yfiles.type="portuserdata"/>
  <key attr.name="url" attr.type="string" for="node" id="d4"/>
  <key attr.name="description" attr.type="string" for="node" id="d5"/>
  <key for="node" id="d6" yfiles.type="nodegraphics"/>
  <key for="graphml" id="d7" yfiles.type="resources"/>
  <key attr.name="url" attr.type="string" for="edge" id="d8"/>
  <key attr.name="description" attr.type="string" for="edge" id="d9"/>
  <key for="edge" id="d10" yfiles.type="edgegraphics"/>
  <graph edgedefault="directed" id="G">
    <data key="d0"/>
	"""
	return out

def yed_graphml_tail():
	out="""  </graph>
  <data key="d7">
    <y:Resources>
	###RESOURCES###
	</y:Resources>
  </data>
</graphml>
	"""
	return out
