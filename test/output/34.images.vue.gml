graph [
  node [
    id 0
    label "10"
    LabelGraphics [
      text "Nodo1.1"
    ]
    graphics [
      x -96.51775
      y 213.06458
      w 138.0
      height 157.0
    ]
  ]
  node [
    id 1
    label "11"
    LabelGraphics [
      text "Nodo&#10;Principale&#10;"
    ]
    graphics [
      x 68.06678
      y -48.895576
      w 138.0
      height 187.0
    ]
  ]
  node [
    id 2
    label "12"
    LabelGraphics [
      text "Nodo1.2"
    ]
    graphics [
      x 195.40009
      y 217.06458
      w 138.0
      height 157.0
    ]
  ]
  node [
    id 3
    label "13"
    LabelGraphics [
      text "Nodo&#10;Scollegato"
    ]
    graphics [
      x 371.0668
      y -29.062206
      w 215.99997
      height 99.33333
    ]
  ]
  edge [
    source 0
    target 1
    label "Conduce&#10;a"
  ]
  edge [
    source 1
    target 2
  ]
]
