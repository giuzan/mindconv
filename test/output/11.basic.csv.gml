graph [
  node [
    id 0
    label "10"
    LabelGraphics [
      text "Miletus"
    ]
    graphics [
    ]
  ]
  node [
    id 1
    label "11"
    LabelGraphics [
      text "Thales&#10;of&#10;Miletus"
    ]
    graphics [
    ]
  ]
  node [
    id 2
    label "12"
    LabelGraphics [
      text "Anaximander"
    ]
    graphics [
    ]
  ]
  node [
    id 3
    label "13"
    LabelGraphics [
      text "Anaxagoras"
    ]
    graphics [
    ]
  ]
  node [
    id 4
    label "14"
    LabelGraphics [
      text "Water"
    ]
    graphics [
    ]
  ]
  node [
    id 5
    label "15"
    LabelGraphics [
      text "Agrigento"
    ]
    graphics [
    ]
  ]
  node [
    id 6
    label "16"
    LabelGraphics [
      text "Athens"
    ]
    graphics [
    ]
  ]
  node [
    id 7
    label "17"
    LabelGraphics [
      text "Persian&#10;War"
    ]
    graphics [
    ]
  ]
  node [
    id 8
    label "18"
    LabelGraphics [
      text "Infinity"
    ]
    graphics [
    ]
  ]
  node [
    id 9
    label "19"
    LabelGraphics [
      text "Firts&#10;Philosofer"
    ]
    graphics [
    ]
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 6
    target 7
  ]
]
