graph [
  node [
    id 0
    label "10"
    LabelGraphics [
      text "Ancient&#10;Greece"
    ]
    graphics [
    ]
  ]
  node [
    id 1
    label "11"
    LabelGraphics [
      text "War"
    ]
    graphics [
    ]
  ]
  node [
    id 2
    label "12"
    LabelGraphics [
      text "Trojan&#10;War"
    ]
    graphics [
    ]
  ]
  node [
    id 3
    label "13"
    LabelGraphics [
      text "Greco&#10;Persian&#10;War"
    ]
    graphics [
    ]
  ]
  node [
    id 4
    label "14"
    LabelGraphics [
      text "Peloponnesian&#10;War"
    ]
    graphics [
    ]
  ]
  node [
    id 5
    label "15"
    LabelGraphics [
      text "Culture&#10;and&#10;Society"
    ]
    graphics [
    ]
  ]
  node [
    id 6
    label "16"
    LabelGraphics [
      text "City&#10;states"
    ]
    graphics [
    ]
  ]
  node [
    id 7
    label "17"
    LabelGraphics [
      text "Athenes"
    ]
    graphics [
    ]
  ]
  node [
    id 8
    label "18"
    LabelGraphics [
      text "Sparta"
    ]
    graphics [
    ]
  ]
  node [
    id 9
    label "19"
    LabelGraphics [
      text "Thebe"
    ]
    graphics [
    ]
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
]
