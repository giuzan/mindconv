graph [
  node [
    id 0
    label "10"
    LabelGraphics [
      text "Image&#10;Test"
    ]
    graphics [
    ]
  ]
  node [
    id 1
    label "11"
    LabelGraphics [
      text "JPG"
    ]
    graphics [
    ]
  ]
  node [
    id 2
    label "12"
    LabelGraphics [
      text "Nato&#10;nel&#10;1879"
    ]
    graphics [
    ]
  ]
  node [
    id 3
    label "13"
    LabelGraphics [
      text "1905:&#10;teoria&#10;della&#10;relativit&#224;&#10;ristretta"
    ]
    graphics [
    ]
  ]
  node [
    id 4
    label "14"
    LabelGraphics [
      text "1916:&#10;teoria&#10;della&#10;relativit&#224;&#10;generali"
    ]
    graphics [
    ]
  ]
  node [
    id 5
    label "15"
    LabelGraphics [
      text "morto&#10;nel&#10;1955"
    ]
    graphics [
    ]
  ]
  node [
    id 6
    label "16"
    LabelGraphics [
      text "SVG"
    ]
    graphics [
    ]
  ]
  node [
    id 7
    label "17"
    LabelGraphics [
      text "Idrogeno"
    ]
    graphics [
    ]
  ]
  node [
    id 8
    label "18"
    LabelGraphics [
      text "Elio"
    ]
    graphics [
    ]
  ]
  edge [
    source 0
    target 1
    label ""
  ]
  edge [
    source 0
    target 6
    label ""
  ]
  edge [
    source 1
    target 2
    label ""
  ]
  edge [
    source 1
    target 3
    label ""
  ]
  edge [
    source 1
    target 4
    label ""
  ]
  edge [
    source 1
    target 5
    label ""
  ]
  edge [
    source 6
    target 7
    label ""
  ]
  edge [
    source 6
    target 8
    label ""
  ]
]
