graph [
  node [
    id 0
    label "10"
    LabelGraphics [
      text "La&#10;Rifrazione"
    ]
    graphics [
    ]
  ]
  node [
    id 1
    label "11"
    LabelGraphics [
      text "Introduzione"
    ]
    graphics [
    ]
  ]
  node [
    id 2
    label "12"
    LabelGraphics [
      text "Velocit&#224;&#10;della&#10;luce"
    ]
    graphics [
    ]
  ]
  node [
    id 3
    label "13"
    LabelGraphics [
      text "Quanto&#10;tempo&#10;impiega&#10;per&#10;il&#10;giro&#10;del&#10;mondo?"
    ]
    graphics [
    ]
  ]
  node [
    id 4
    label "14"
    LabelGraphics [
      text "dimostr&#242;&#10;che&#10;era&#10;molto&#10;grande&#10;o&#10;infinita"
    ]
    graphics [
    ]
  ]
  edge [
    source 0
    target 1
    label ""
  ]
  edge [
    source 1
    target 2
    label ""
  ]
  edge [
    source 2
    target 3
    label ""
  ]
  edge [
    source 2
    target 4
    label ""
  ]
]
