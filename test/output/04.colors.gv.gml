graph [
  directed 1
  multigraph 1
  name "G"
  graph [
  ]
  node [
    id 0
    label "10"
    color "chocolate"
    LabelGraphics [
      text "a"
    ]
    graphics [
    ]
  ]
  node [
    id 1
    label "11"
    color "green"
    fillcolor "pink"
    style "filled"
    LabelGraphics [
      text "b"
    ]
    graphics [
    ]
  ]
  node [
    id 2
    label "12"
    color "yellow"
    style "filled"
    LabelGraphics [
      text "c"
    ]
    graphics [
    ]
  ]
  node [
    id 3
    label "13"
    style "filled"
    LabelGraphics [
      text "d"
    ]
    graphics [
    ]
  ]
  node [
    id 4
    label "14"
    color "#40e0d0"
    fillcolor "0.051 0.718 0.627"
    style "filled"
    LabelGraphics [
      text "f"
    ]
    graphics [
    ]
  ]
  node [
    id 5
    label "15"
    color "#ffff00"
    LabelGraphics [
      text "g"
    ]
    graphics [
    ]
  ]
  node [
    id 6
    label "16"
    LabelGraphics [
      text "e"
    ]
    graphics [
    ]
  ]
  node [
    id 7
    label "17"
    LabelGraphics [
      text "i"
    ]
    graphics [
    ]
  ]
  edge [
    source 0
    target 1
    key 0
  ]
  edge [
    source 0
    target 2
    key 0
  ]
  edge [
    source 0
    target 3
    key 0
  ]
  edge [
    source 0
    target 4
    key 0
  ]
  edge [
    source 0
    target 5
    key 0
  ]
  edge [
    source 0
    target 6
    key 0
  ]
  edge [
    source 7
    target 1
    key 0
    color "red"
  ]
  edge [
    source 7
    target 3
    key 0
    color "#ffff00"
  ]
  edge [
    source 7
    target 3
    key 1
    color "0.051 0.718 0.627"
  ]
]
