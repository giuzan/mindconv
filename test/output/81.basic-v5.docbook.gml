graph [
  node [
    id 0
    label "10"
    LabelGraphics [
      text "Ancient&#10;Greek"
    ]
    graphics [
    ]
  ]
  node [
    id 1
    label "11"
    LabelGraphics [
      text "Miletus"
    ]
    graphics [
    ]
  ]
  node [
    id 2
    label "12"
    LabelGraphics [
      text "Thales&#10;of&#10;Miletus"
    ]
    graphics [
    ]
  ]
  node [
    id 3
    label "13"
    LabelGraphics [
      text "Anaximander"
    ]
    graphics [
    ]
  ]
  node [
    id 4
    label "14"
    LabelGraphics [
      text "Firts&#10;Philosofer"
    ]
    graphics [
    ]
  ]
  node [
    id 5
    label "15"
    LabelGraphics [
      text "Infinity"
    ]
    graphics [
    ]
  ]
  node [
    id 6
    label "16"
    LabelGraphics [
      text "Water"
    ]
    graphics [
    ]
  ]
  node [
    id 7
    label "17"
    LabelGraphics [
      text "Anaxagoras"
    ]
    graphics [
    ]
  ]
  node [
    id 8
    label "18"
    LabelGraphics [
      text "Agrigento"
    ]
    graphics [
    ]
  ]
  node [
    id 9
    label "19"
    LabelGraphics [
      text "Athens"
    ]
    graphics [
    ]
  ]
  node [
    id 10
    label "20"
    LabelGraphics [
      text "Persian&#10;War"
    ]
    graphics [
    ]
  ]
  edge [
    source 0
    target 1
    label ""
  ]
  edge [
    source 0
    target 8
    label ""
  ]
  edge [
    source 0
    target 9
    label ""
  ]
  edge [
    source 1
    target 2
    label ""
  ]
  edge [
    source 1
    target 3
    label ""
  ]
  edge [
    source 1
    target 7
    label ""
  ]
  edge [
    source 3
    target 4
    label ""
  ]
  edge [
    source 3
    target 5
    label ""
  ]
  edge [
    source 3
    target 6
    label ""
  ]
  edge [
    source 9
    target 10
    label ""
  ]
]
