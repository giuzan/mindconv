graph [
  node [
    id 0
    label "10"
    LabelGraphics [
      text "Come&#10;costruire&#10;un&#10;universo"
    ]
    graphics [
    ]
  ]
  node [
    id 1
    label "11"
    LabelGraphics [
      text "Georges&#10;Lema&#238;tre"
    ]
    graphics [
    ]
  ]
  node [
    id 2
    label "12"
    LabelGraphics [
      text "1894&#10;&#8211;&#10;1966&#10;&#232;&#10;stato&#10;un&#10;fisico,&#10;astronomo&#10;e&#10;presbitero&#10;belga."
    ]
    graphics [
    ]
  ]
  node [
    id 3
    label "13"
    LabelGraphics [
      text "Spostamento&#10;verso&#10;il&#10;rosso&#10;&#8594;&#10;Espansione&#10;universo"
    ]
    graphics [
    ]
  ]
  node [
    id 4
    label "14"
    LabelGraphics [
      text "Negli&#10;anni&#10;venti&#10;ipotizzo&#10;il&#10;big&#10;bang"
    ]
    graphics [
    ]
  ]
  node [
    id 5
    label "15"
    LabelGraphics [
      text "Big&#10;Bang"
    ]
    graphics [
    ]
  ]
  node [
    id 6
    label "16"
    LabelGraphics [
      text "13,7&#10;miliardi&#10;di&#10;anni&#10;fa"
    ]
    graphics [
    ]
  ]
  node [
    id 7
    label "17"
    LabelGraphics [
      text "Prime&#10;teorie&#10;non&#10;riuscivano&#10;a&#10;spiegare&#10;creazione&#10;elementi&#10;pesanti&#160;"
    ]
    graphics [
    ]
  ]
  node [
    id 8
    label "18"
    LabelGraphics [
      text "Arno&#10;Penzias&#10;e&#10;Robert&#10;Wilson"
    ]
    graphics [
    ]
  ]
  node [
    id 9
    label "19"
    LabelGraphics [
      text "Penzias&#10;1933&#10;-&#10;&#232;&#10;un&#10;fisico&#10;statunitense."
    ]
    graphics [
    ]
  ]
  node [
    id 10
    label "20"
    LabelGraphics [
      text "Wilson&#10;1936&#10;-&#10;&#232;&#10;un&#10;astronomo&#10;e&#10;fisico&#10;statunitense."
    ]
    graphics [
    ]
  ]
  node [
    id 11
    label "21"
    LabelGraphics [
      text "Trovarono&#10;il&#10;rumore&#10;di&#10;fondo&#10;su&#10;antenna"
    ]
    graphics [
    ]
  ]
  node [
    id 12
    label "22"
    LabelGraphics [
      text "Robert&#10;Dicke"
    ]
    graphics [
    ]
  ]
  node [
    id 13
    label "23"
    LabelGraphics [
      text "1916&#10;-&#10;1997&#10;&#232;&#10;stato&#10;un&#10;fisico&#10;sperimentale&#10;statunitense"
    ]
    graphics [
    ]
  ]
  node [
    id 14
    label "24"
    LabelGraphics [
      text "contributi&#10;nel&#10;campo&#10;dell&#8217;astrofisica,&#10;fisica&#10;atomica,&#10;cosmologia&#10;e&#10;gravit&#224;."
    ]
    graphics [
    ]
  ]
  node [
    id 15
    label "25"
    LabelGraphics [
      text "Stava&#10;studiando&#10;a&#10;Princeton&#10;l&#8217;ipotesi&#10;di&#10;Gamow&#10;della&#10;radiazione&#10;di&#10;fondo"
    ]
    graphics [
    ]
  ]
  node [
    id 16
    label "26"
    LabelGraphics [
      text "George&#10;Gamow"
    ]
    graphics [
    ]
  ]
  node [
    id 17
    label "27"
    LabelGraphics [
      text "1904&#10;-&#10;1968&#10;&#232;&#10;stato&#10;un&#10;fisico,&#10;cosmologo&#10;e&#10;divulgatore&#10;scientifico&#10;russo&#10;naturalizzato&#10;statunitense."
    ]
    graphics [
    ]
  ]
  node [
    id 18
    label "28"
    LabelGraphics [
      text "Ipotizz&#242;&#10;che&#10;scrutando&#10;l&#8217;universo&#10;si&#10;sarbbe&#10;trovata&#10;la&#10;radiazione&#10;di&#10;fondo"
    ]
    graphics [
    ]
  ]
  node [
    id 19
    label "29"
    LabelGraphics [
      text "Alan&#10;Guth"
    ]
    graphics [
    ]
  ]
  node [
    id 20
    label "30"
    LabelGraphics [
      text "1947&#10;-&#10;&#232;&#10;un&#10;fisico&#10;e&#10;cosmologo&#10;statunitense."
    ]
    graphics [
    ]
  ]
  node [
    id 21
    label "31"
    LabelGraphics [
      text "Propose&#10;l&#8217;universo&#10;inflazionario"
    ]
    graphics [
    ]
  ]
  edge [
    source 0
    target 1
    label ""
  ]
  edge [
    source 0
    target 5
    label ""
  ]
  edge [
    source 0
    target 8
    label ""
  ]
  edge [
    source 0
    target 12
    label ""
  ]
  edge [
    source 0
    target 16
    label ""
  ]
  edge [
    source 0
    target 19
    label ""
  ]
  edge [
    source 1
    target 2
    label ""
  ]
  edge [
    source 1
    target 3
    label ""
  ]
  edge [
    source 1
    target 4
    label ""
  ]
  edge [
    source 5
    target 6
    label ""
  ]
  edge [
    source 5
    target 7
    label ""
  ]
  edge [
    source 8
    target 9
    label ""
  ]
  edge [
    source 8
    target 10
    label ""
  ]
  edge [
    source 8
    target 11
    label ""
  ]
  edge [
    source 12
    target 13
    label ""
  ]
  edge [
    source 12
    target 14
    label ""
  ]
  edge [
    source 12
    target 15
    label ""
  ]
  edge [
    source 16
    target 17
    label ""
  ]
  edge [
    source 16
    target 18
    label ""
  ]
  edge [
    source 19
    target 20
    label ""
  ]
  edge [
    source 19
    target 21
    label ""
  ]
]
