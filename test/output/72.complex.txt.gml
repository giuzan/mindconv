graph [
  node [
    id 0
    label "10"
    LabelGraphics [
      text "Ancient&#10;Greece"
    ]
    graphics [
    ]
  ]
  node [
    id 1
    label "11"
    LabelGraphics [
      text "War"
    ]
    graphics [
    ]
  ]
  node [
    id 2
    label "12"
    LabelGraphics [
      text "Trojan&#10;War"
    ]
    graphics [
    ]
  ]
  node [
    id 3
    label "13"
    LabelGraphics [
      text "Greco&#10;Persian&#10;War"
    ]
    graphics [
    ]
  ]
  node [
    id 4
    label "14"
    LabelGraphics [
      text "Peloponnesian&#10;War"
    ]
    graphics [
    ]
  ]
  node [
    id 5
    label "15"
    LabelGraphics [
      text "Culture\nSociety"
    ]
    graphics [
    ]
  ]
  node [
    id 6
    label "16"
    LabelGraphics [
      text "City&#10;states"
    ]
    graphics [
    ]
  ]
  node [
    id 7
    label "17"
    LabelGraphics [
      text "Athenes"
    ]
    graphics [
    ]
  ]
  node [
    id 8
    label "18"
    LabelGraphics [
      text "Sparta"
    ]
    graphics [
    ]
  ]
  node [
    id 9
    label "19"
    LabelGraphics [
      text "Thebe"
    ]
    graphics [
    ]
  ]
  node [
    id 10
    label "20"
    LabelGraphics [
      text "People"
    ]
    graphics [
    ]
  ]
  node [
    id 11
    label "21"
    LabelGraphics [
      text "Pericles\n&#928;&#949;&#961;&#953;&#954;&#955;&#8134;&#962;"
    ]
    graphics [
    ]
  ]
  node [
    id 12
    label "22"
    LabelGraphics [
      text "Aristotele\n&#7944;&#961;&#953;&#963;&#964;&#959;&#964;&#941;&#955;&#951;&#962;"
    ]
    graphics [
    ]
  ]
  node [
    id 13
    label "23"
    LabelGraphics [
      text "History&#10;/&#10;Art"
    ]
    graphics [
    ]
  ]
  node [
    id 14
    label "24"
    LabelGraphics [
      text "Pantheon"
    ]
    graphics [
    ]
  ]
  node [
    id 15
    label "25"
    LabelGraphics [
      text "Persian"
    ]
    graphics [
    ]
  ]
  node [
    id 16
    label "26"
    LabelGraphics [
      text "Name"
    ]
    graphics [
    ]
  ]
  node [
    id 17
    label "27"
    LabelGraphics [
      text "&#1588;&#1575;&#1607;&#1606;&#1588;&#1575;&#1607;&#1740;&#10;&#1575;&#1740;&#1585;&#1575;&#1606;&#8206;"
    ]
    graphics [
    ]
  ]
  node [
    id 18
    label "28"
    LabelGraphics [
      text "People"
    ]
    graphics [
    ]
  ]
  node [
    id 19
    label "29"
    LabelGraphics [
      text "Cyrus&#10;the&#10;Great"
    ]
    graphics [
    ]
  ]
  node [
    id 20
    label "30"
    LabelGraphics [
      text "Darius&#10;III"
    ]
    graphics [
    ]
  ]
  node [
    id 21
    label "31"
    LabelGraphics [
      text "War"
    ]
    graphics [
    ]
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
]
