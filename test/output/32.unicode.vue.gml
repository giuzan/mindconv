graph [
  node [
    id 0
    label "10"
    LabelGraphics [
      text "A1"
    ]
    graphics [
      x 192.75
      y 64.0
      w 136.0
      height 82.0
    ]
  ]
  node [
    id 1
    label "11"
    LabelGraphics [
      text "A2"
    ]
    graphics [
      x 247.25
      y 192.0
      w 27.0
      height 23.0
    ]
  ]
  node [
    id 2
    label "12"
    LabelGraphics [
      text "A2"
    ]
    graphics [
      x 435.5
      y 185.5
      w 93.0
      height 41.0
    ]
  ]
  node [
    id 3
    label "13"
    LabelGraphics [
      text "A2.1"
    ]
    graphics [
      x 212.0
      y 261.0
      w 39.0
      height 23.0
    ]
  ]
  node [
    id 4
    label "14"
    LabelGraphics [
      text "A2.2"
    ]
    graphics [
      x 270.5
      y 261.0
      w 39.0
      height 23.0
    ]
  ]
  node [
    id 5
    label "15"
    LabelGraphics [
      text "Unicode&#10;Test&#10;on&#10;more&#10;rows&#10;&#232;&#232;&#10;-&#10;&#224;&#224;"
    ]
    graphics [
      x 323.5
      y 332.0
      w 98.125
      height 83.0
    ]
  ]
  edge [
    source 0
    target 1
    label "Include"
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 4
    target 5
  ]
]
