graph [
  node [
    id 0
    label "10"
    LabelGraphics [
      text "roundRect"
    ]
    graphics [
      x 334.0
      y 225.0
      w 141.0
      height 111.0
    ]
  ]
  node [
    id 1
    label "11"
    LabelGraphics [
      text "hexagon"
    ]
    graphics [
      x 627.5
      y 277.5
      w 85.1
      height 33.100002
    ]
  ]
  node [
    id 2
    label "12"
    LabelGraphics [
      text "diamond"
    ]
    graphics [
      x 382.5
      y 409.5
      w 100.0
      height 47.0
    ]
  ]
  node [
    id 3
    label "13"
    LabelGraphics [
      text "ellipse"
    ]
    graphics [
      x 162.5
      y 377.5
      w 63.6
      height 26.600002
    ]
  ]
  node [
    id 4
    label "14"
    LabelGraphics [
      text "rectangle"
    ]
    graphics [
      x 69.5
      y 293.5
      w 76.0
      height 23.0
    ]
  ]
  node [
    id 5
    label "15"
    LabelGraphics [
      text "flag2"
    ]
    graphics [
      x 712.5
      y 397.0
      w 66.4
      height 39.4
    ]
  ]
  node [
    id 6
    label "16"
    LabelGraphics [
      text "flag"
    ]
    graphics [
      x 327.5
      y 510.0
      w 57.8
      height 38.8
    ]
  ]
  node [
    id 7
    label "17"
    LabelGraphics [
      text "octagon"
    ]
    graphics [
      x 82.5
      y 527.0
      w 74.6
      height 26.6
    ]
  ]
  node [
    id 8
    label "18"
    LabelGraphics [
      text "triangle"
    ]
    graphics [
      x -55.5
      y 402.5
      w 94.200005
      height 47.2
    ]
  ]
  node [
    id 9
    label "19"
    LabelGraphics [
      text "chevron"
    ]
    graphics [
      x 774.5
      y 538.5
      w 78.9
      height 30.900002
    ]
  ]
  node [
    id 10
    label "20"
    LabelGraphics [
      text "rhombus"
    ]
    graphics [
      x 368.5
      y 626.5
      w 95.6
      height 41.6
    ]
  ]
  node [
    id 11
    label "21"
    LabelGraphics [
      text "shield"
    ]
    graphics [
      x 104.5
      y 605.5
      w 77.4
      height 43.4
    ]
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 7
    target 11
  ]
]
