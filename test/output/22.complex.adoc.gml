graph [
  node [
    id 0
    label "10"
    LabelGraphics [
      text "Ancient&#10;Greece"
    ]
    graphics [
    ]
  ]
  node [
    id 1
    label "11"
    LabelGraphics [
      text "War"
    ]
    graphics [
    ]
  ]
  node [
    id 2
    label "12"
    LabelGraphics [
      text "Warfare&#10;occurred&#10;throughout&#10;the&#10;history&#10;of&#10;Ancient&#10;Greece,&#10;from&#10;the&#10;Greek&#10;Dark&#10;Ages&#10;onward."
    ]
    graphics [
    ]
  ]
  node [
    id 3
    label "13"
    LabelGraphics [
      text "Trojan&#10;War"
    ]
    graphics [
    ]
  ]
  node [
    id 4
    label "14"
    LabelGraphics [
      text "Greco&#10;Persian&#10;War"
    ]
    graphics [
    ]
  ]
  node [
    id 5
    label "15"
    LabelGraphics [
      text "Peloponnesian&#10;War"
    ]
    graphics [
    ]
  ]
  node [
    id 6
    label "16"
    LabelGraphics [
      text "Culture&#10;Society"
    ]
    graphics [
    ]
  ]
  node [
    id 7
    label "17"
    LabelGraphics [
      text "City&#10;states"
    ]
    graphics [
    ]
  ]
  node [
    id 8
    label "18"
    LabelGraphics [
      text "Athenes"
    ]
    graphics [
    ]
  ]
  node [
    id 9
    label "19"
    LabelGraphics [
      text "Sparta"
    ]
    graphics [
    ]
  ]
  node [
    id 10
    label "20"
    LabelGraphics [
      text "Thebe"
    ]
    graphics [
    ]
  ]
  node [
    id 11
    label "21"
    LabelGraphics [
      text "People"
    ]
    graphics [
    ]
  ]
  node [
    id 12
    label "22"
    LabelGraphics [
      text "Pericles&#10;&#928;&#949;&#961;&#953;&#954;&#955;&#8134;&#962;"
    ]
    graphics [
    ]
  ]
  node [
    id 13
    label "23"
    LabelGraphics [
      text "Aristotele&#10;&#7944;&#961;&#953;&#963;&#964;&#959;&#964;&#941;&#955;&#951;&#962;"
    ]
    graphics [
    ]
  ]
  node [
    id 14
    label "24"
    LabelGraphics [
      text "History&#10;/&#10;Art"
    ]
    graphics [
    ]
  ]
  node [
    id 15
    label "25"
    LabelGraphics [
      text "Pantheon"
    ]
    graphics [
    ]
  ]
  edge [
    source 0
    target 1
    label ""
  ]
  edge [
    source 0
    target 6
    label ""
  ]
  edge [
    source 0
    target 11
    label ""
  ]
  edge [
    source 0
    target 14
    label ""
  ]
  edge [
    source 1
    target 2
    label ""
  ]
  edge [
    source 1
    target 3
    label ""
  ]
  edge [
    source 1
    target 4
    label ""
  ]
  edge [
    source 1
    target 5
    label ""
  ]
  edge [
    source 6
    target 7
    label "Typically"
  ]
  edge [
    source 7
    target 8
    label ""
  ]
  edge [
    source 7
    target 9
    label ""
  ]
  edge [
    source 7
    target 10
    label ""
  ]
  edge [
    source 11
    target 12
    label ""
  ]
  edge [
    source 11
    target 13
    label ""
  ]
  edge [
    source 14
    target 15
    label ""
  ]
]
