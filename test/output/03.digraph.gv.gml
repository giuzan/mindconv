graph [
  directed 1
  multigraph 1
  name "G"
  graph [
  ]
  node [
    id 0
    label "10"
    LabelGraphics [
      text "One"
    ]
    graphics [
    ]
  ]
  node [
    id 1
    label "11"
    LabelGraphics [
      text "Two"
    ]
    graphics [
    ]
  ]
  node [
    id 2
    label "12"
    LabelGraphics [
      text "Three"
    ]
    graphics [
    ]
  ]
  edge [
    source 0
    target 1
    key 0
  ]
  edge [
    source 1
    target 2
    key 0
  ]
]
