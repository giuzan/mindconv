graph [
  multigraph 1
  name "shapes"
  label ""
  graph [
    label ""
  ]
  node [
    id 0
    label "10"
    shape "diamond"
    LabelGraphics [
      text "diamond"
    ]
    graphics [
    ]
  ]
  node [
    id 1
    label "11"
    shape "Mdiamond"
    LabelGraphics [
      text "Mdiamond"
    ]
    graphics [
    ]
  ]
  node [
    id 2
    label "12"
    shape "ellipse"
    LabelGraphics [
      text "ellipse"
    ]
    graphics [
    ]
  ]
  node [
    id 3
    label "13"
    shape "Mcircle"
    LabelGraphics [
      text "Mcircle"
    ]
    graphics [
    ]
  ]
  node [
    id 4
    label "14"
    shape "circle"
    LabelGraphics [
      text "circle"
    ]
    graphics [
    ]
  ]
  node [
    id 5
    label "15"
    shape "doublecircle"
    LabelGraphics [
      text "doublecircle"
    ]
    graphics [
    ]
  ]
  node [
    id 6
    label "16"
    shape "egg"
    LabelGraphics [
      text "egg"
    ]
    graphics [
    ]
  ]
  node [
    id 7
    label "17"
    shape "oval"
    LabelGraphics [
      text "oval"
    ]
    graphics [
    ]
  ]
  node [
    id 8
    label "18"
    shape "ellipse"
    LabelGraphics [
      text "point"
    ]
    graphics [
    ]
  ]
  node [
    id 9
    label "19"
    shape "hexagon"
    LabelGraphics [
      text "hexagon"
    ]
    graphics [
    ]
  ]
  node [
    id 10
    label "20"
    shape "pentagon"
    LabelGraphics [
      text "pentagon"
    ]
    graphics [
    ]
  ]
  node [
    id 11
    label "21"
    shape "octagon"
    LabelGraphics [
      text "octagon"
    ]
    graphics [
    ]
  ]
  node [
    id 12
    label "22"
    shape "doubleoctagon"
    LabelGraphics [
      text "doubleoctagon"
    ]
    graphics [
    ]
  ]
  node [
    id 13
    label "23"
    shape "tripleoctagon"
    LabelGraphics [
      text "tripleoctagon"
    ]
    graphics [
    ]
  ]
  node [
    id 14
    label "24"
    shape "septagon"
    LabelGraphics [
      text "septagon"
    ]
    graphics [
    ]
  ]
  node [
    id 15
    label "25"
    shape "box"
    LabelGraphics [
      text "box"
    ]
    graphics [
    ]
  ]
  node [
    id 16
    label "26"
    shape "plaintext"
    LabelGraphics [
      text "plaintext"
    ]
    graphics [
    ]
  ]
  node [
    id 17
    label "27"
    shape "Msquare"
    LabelGraphics [
      text "Msquare"
    ]
    graphics [
    ]
  ]
  node [
    id 18
    label "28"
    shape "rect"
    LabelGraphics [
      text "rect"
    ]
    graphics [
    ]
  ]
  node [
    id 19
    label "29"
    shape "rectangle"
    LabelGraphics [
      text "rectangle"
    ]
    graphics [
    ]
  ]
  node [
    id 20
    label "30"
    shape "square"
    LabelGraphics [
      text "square"
    ]
    graphics [
    ]
  ]
  node [
    id 21
    label "31"
    shape "none"
    LabelGraphics [
      text "none"
    ]
    graphics [
    ]
  ]
  node [
    id 22
    label "32"
    shape "note"
    LabelGraphics [
      text "note"
    ]
    graphics [
    ]
  ]
  node [
    id 23
    label "33"
    shape "tab"
    LabelGraphics [
      text "tab"
    ]
    graphics [
    ]
  ]
  node [
    id 24
    label "34"
    shape "folder"
    LabelGraphics [
      text "folder"
    ]
    graphics [
    ]
  ]
  node [
    id 25
    label "35"
    shape "box3d"
    LabelGraphics [
      text "box3d"
    ]
    graphics [
    ]
  ]
  node [
    id 26
    label "36"
    shape "component"
    LabelGraphics [
      text "component"
    ]
    graphics [
    ]
  ]
  node [
    id 27
    label "37"
    shape "parallelogram"
    LabelGraphics [
      text "parallelogram"
    ]
    graphics [
    ]
  ]
  node [
    id 28
    label "38"
    shape "polygon"
    LabelGraphics [
      text "polygon"
    ]
    graphics [
    ]
  ]
  node [
    id 29
    label "39"
    shape "triangle"
    LabelGraphics [
      text "triangle"
    ]
    graphics [
    ]
  ]
  node [
    id 30
    label "40"
    shape "house"
    LabelGraphics [
      text "house"
    ]
    graphics [
    ]
  ]
  node [
    id 31
    label "41"
    shape "trapezium"
    LabelGraphics [
      text "trapezium"
    ]
    graphics [
    ]
  ]
  node [
    id 32
    label "42"
    shape "invtriangle"
    LabelGraphics [
      text "invtriangle"
    ]
    graphics [
    ]
  ]
  node [
    id 33
    label "43"
    shape "invtrapezium"
    LabelGraphics [
      text "invtrapezium"
    ]
    graphics [
    ]
  ]
  node [
    id 34
    label "44"
    shape "invhouse"
    LabelGraphics [
      text "invhouse"
    ]
    graphics [
    ]
  ]
  edge [
    source 0
    target 1
    key 0
  ]
  edge [
    source 0
    target 1
    key 1
  ]
  edge [
    source 2
    target 3
    key 0
  ]
  edge [
    source 2
    target 8
    key 0
  ]
  edge [
    source 3
    target 4
    key 0
  ]
  edge [
    source 4
    target 5
    key 0
  ]
  edge [
    source 5
    target 6
    key 0
  ]
  edge [
    source 6
    target 7
    key 0
  ]
  edge [
    source 7
    target 8
    key 0
  ]
  edge [
    source 9
    target 10
    key 0
  ]
  edge [
    source 9
    target 10
    key 1
  ]
  edge [
    source 11
    target 12
    key 0
  ]
  edge [
    source 11
    target 14
    key 0
  ]
  edge [
    source 12
    target 13
    key 0
  ]
  edge [
    source 13
    target 14
    key 0
  ]
  edge [
    source 15
    target 25
    key 0
  ]
  edge [
    source 15
    target 18
    key 0
  ]
  edge [
    source 16
    target 24
    key 0
  ]
  edge [
    source 16
    target 25
    key 0
  ]
  edge [
    source 17
    target 23
    key 0
  ]
  edge [
    source 17
    target 20
    key 0
  ]
  edge [
    source 18
    target 19
    key 0
  ]
  edge [
    source 19
    target 26
    key 0
  ]
  edge [
    source 20
    target 22
    key 0
  ]
  edge [
    source 21
    target 22
    key 0
  ]
  edge [
    source 21
    target 24
    key 0
  ]
  edge [
    source 23
    target 26
    key 0
  ]
  edge [
    source 27
    target 28
    key 0
  ]
  edge [
    source 27
    target 28
    key 1
  ]
  edge [
    source 29
    target 30
    key 0
  ]
  edge [
    source 29
    target 31
    key 0
  ]
  edge [
    source 30
    target 31
    key 0
  ]
  edge [
    source 32
    target 34
    key 0
  ]
  edge [
    source 32
    target 33
    key 0
  ]
  edge [
    source 33
    target 34
    key 0
  ]
]
