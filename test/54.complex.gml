Creator	"yFiles"
Version	"2.14"
graph
[
	hierarchic	1
	label	""
	directed	1
	node
	[
		id	0
		label	"Vulcano Etna"
		graphics
		[
			x	196.089111328125
			y	119.4375
			w	270.0
			h	210.9375
			type	"rectangle"
			raisedBorder	0
			fill	"#99CCFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"Vulcano Etna"
			fontSize	24
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	1
		label	"Parti"
		graphics
		[
			x	342.03076171875
			y	299.84375
			w	41.5625
			h	30.0
			type	"rectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"Parti"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	2
		label	"Interno"
		graphics
		[
			x	222.7060546875
			y	369.84375
			w	57.11328125
			h	30.0
			type	"rectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"Interno"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	3
		label	"Esterno"
		graphics
		[
			x	461.35546875
			y	369.84375
			w	59.802734375
			h	30.0
			type	"rectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"Esterno"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	4
		label	"Camino"
		graphics
		[
			x	132.67578125
			y	445.8125
			w	59.703125
			h	30.0
			type	"rectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"Camino"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	5
		label	"Camera
Magmatica"
		graphics
		[
			x	222.7060546875
			y	445.8125
			w	80.357421875
			h	41.9375
			type	"rectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"Camera
Magmatica"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	6
		label	"Cratere"
		graphics
		[
			x	312.419921875
			y	445.8125
			w	59.0703125
			h	30.0
			type	"rectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"Cratere"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	7
		label	"Cono"
		graphics
		[
			x	461.35546875
			y	439.84375
			w	44.66796875
			h	30.0
			type	"rectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"Cono"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	8
		label	"Lapilli"
		graphics
		[
			x	528.185546875
			y	439.84375
			w	48.9921875
			h	30.0
			type	"rectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"Lapilli"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	9
		label	"Cenere"
		graphics
		[
			x	390.48828125
			y	439.84375
			w	57.06640625
			h	30.0
			type	"rectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"Cenere"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	10
		label	"Antichi
Greci"
		graphics
		[
			x	50.1474609375
			y	299.84375
			w	55.390625
			h	41.9375
			type	"rectangle"
			raisedBorder	0
			fill	"#CCFFCC"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"Antichi
Greci"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	11
		label	"Dei"
		graphics
		[
			x	16.978515625
			y	375.8125
			w	33.95703125
			h	30.0
			type	"rectangle"
			raisedBorder	0
			fill	"#CCFFCC"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"Dei"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	12
		label	"Colonie"
		graphics
		[
			x	83.31640625
			y	375.8125
			w	58.71875
			h	30.0
			type	"rectangle"
			raisedBorder	0
			fill	"#CCFFCC"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"Colonie"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	edge
	[
		source	0
		target	1
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	1
		target	2
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	1
		target	3
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	2
		target	4
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	2
		target	5
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	2
		target	6
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	3
		target	7
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	3
		target	8
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	3
		target	9
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	0
		target	10
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	10
		target	12
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	10
		target	11
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
]
