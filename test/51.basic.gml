Creator	"yFiles"
Version	"2.14"
graph
[
	hierarchic	1
	label	""
	directed	1
	node
	[
		id	0
		label	"A1"
		graphics
		[
			x	288.0
			y	165.0
			w	29.84375
			h	30.0
			type	"roundrectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"A1"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	1
		label	"A1.1"
		graphics
		[
			x	197.0
			y	231.0
			w	41.29296875
			h	30.0
			type	"rectangle3d"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"A1.1"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	2
		label	"A2.1"
		graphics
		[
			x	398.0
			y	235.0
			w	41.29296875
			h	30.0
			customconfiguration	"BevelNode3"
			fill	"#FF9900"
			hasOutline	0
		]
		LabelGraphics
		[
			text	"A2.1"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	3
		label	"A1.1.1"
		graphics
		[
			x	158.0
			y	334.0
			w	52.7421875
			h	30.0
			type	"roundrectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"A1.1.1"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	4
		label	"A1.1.2"
		graphics
		[
			x	298.0
			y	341.0
			w	52.7421875
			h	30.0
			type	"roundrectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"A1.1.2"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	edge
	[
		source	0
		target	1
		label	"Brings to"
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"Brings to"
			fontSize	12
			fontName	"Dialog"
			configuration	"AutoFlippingLabel"
			contentWidth	57.8359375
			contentHeight	17.96875
			model	"null"
			position	"null"
		]
	]
	edge
	[
		source	1
		target	3
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	1
		target	4
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	0
		target	2
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
	]
]
