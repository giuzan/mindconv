Creator	"yFiles"
Version	"2.14"
graph
[
	hierarchic	1
	label	""
	directed	1
	node
	[
		id	0
		label	"Mappa"
		graphics
		[
			x	628.6666666666666
			y	163.0
			w	54.294921875
			h	30.0
			type	"roundrectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"Mappa"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	1
		label	"A1"
		graphics
		[
			x	516.0
			y	297.5
			w	29.84375
			h	30.0
			type	"roundrectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"A1"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	2
		label	"A2"
		graphics
		[
			x	774.0
			y	297.5
			w	29.84375
			h	30.0
			type	"roundrectangle"
			raisedBorder	0
			fill	"#FF99CC"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"A2"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	3
		label	"A1.1"
		graphics
		[
			x	410.0
			y	386.0
			w	41.29296875
			h	30.0
			type	"roundrectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"A1.1"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	4
		label	"A1.3"
		graphics
		[
			x	549.0
			y	386.0
			w	41.29296875
			h	30.0
			type	"roundrectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"A1.3"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	5
		label	"A1.2"
		graphics
		[
			x	476.0
			y	386.0
			w	41.29296875
			h	30.0
			type	"roundrectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"A1.2"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	6
		label	"A2.1"
		graphics
		[
			x	739.0
			y	386.0
			w	41.29296875
			h	30.0
			type	"roundrectangle"
			raisedBorder	0
			fill	"#339966"
			fill2	"#808000"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"A2.1"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	7
		label	"A2.2"
		graphics
		[
			x	833.0
			y	386.0
			w	41.29296875
			h	30.0
			type	"roundrectangle"
			raisedBorder	0
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"A2.2"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	8
		label	"Uno"
		graphics
		[
			x	210.66666666666674
			y	228.00000000000003
			w	56.554100036621094
			h	64.63923645019531
			fill	"#CCCCFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"Uno"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	9
		label	""
		graphics
		[
			x	144.0
			y	329.66666666666663
			w	56.554100036621094
			h	66.76200103759766
			fill	"#CCCCFF"
			outline	"#000000"
		]
		LabelGraphics
		[
		]
	]
	node
	[
		id	10
		label	""
		graphics
		[
			x	277.3333333333335
			y	331.88811556498206
			w	56.554100036621094
			h	62.3191032409668
			fill	"#CCCCFF"
			outline	"#000000"
		]
		LabelGraphics
		[
		]
	]
	edge
	[
		source	0
		target	2
		label	"Conduce a"
		graphics
		[
			type	"spline"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	628.6666666666666
					y	163.0
				]
				point
				[
					x	757.3333333333334
					y	204.66666666666669
				]
				point
				[
					x	774.0
					y	297.5
				]
			]
		]
		LabelGraphics
		[
			text	"Conduce a"
			fill	"#FFFFFF"
			fontSize	12
			fontName	"Dialog"
			configuration	"AutoFlippingLabel"
			contentWidth	67.697265625
			contentHeight	17.96875
			model	"null"
			position	"null"
		]
	]
	edge
	[
		source	0
		target	1
		label	"Implica"
		graphics
		[
			type	"spline"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	628.6666666666666
					y	163.0
				]
				point
				[
					x	549.0
					y	209.66666666666669
				]
				point
				[
					x	516.0
					y	297.5
				]
			]
		]
		LabelGraphics
		[
			text	"Implica"
			outline	"#000000"
			fontSize	12
			fontName	"Dialog"
			configuration	"AutoFlippingLabel"
			contentWidth	47.46484375
			contentHeight	17.96875
			model	"null"
			position	"null"
		]
	]
	edge
	[
		source	1
		target	3
		graphics
		[
			type	"spline"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	1
		target	5
		graphics
		[
			type	"spline"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	1
		target	4
		graphics
		[
			type	"spline"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	2
		target	6
		graphics
		[
			type	"spline"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	2
		target	7
		graphics
		[
			type	"spline"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	8
		target	9
		graphics
		[
			type	"spline"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	8
		target	10
		graphics
		[
			type	"spline"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
]
