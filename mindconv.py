#!/usr/bin/python
# -*- coding: utf-8 -*-
# gd 20170321
# :revdate: 20180328


program_description= """
Convert between different format of mind map files
"""

# Part of code based on: dot2vue.py Copyright (c) 2009-2012, Yan Fang, D. Stott Parker

#for dot
#	pip install pygraphviz (for pygraphviz install also sudo pacman -S pkg-config)
#	pip install pydot 

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.


__author__ = 'Giuliano Dedda'
__version__ = '0.2.5'
__license__ = 'MIT'

"""
TODO
High priority:
[x] images
[X] link in adocs with . 

Low Priority:
[ ] input graphml
[ ] input gml
[ ] output format mm (i nodi nel xml sono annidiati uno dentro l'altro ...) -> bisogna cambiare strategia
[ ] input format mm
[ ] there is some error in the graphml export of graphiz format (gv)
[X] move config to an ini file
[ ] SPLIT_NODE_LABEL add 'Auto' (if row is too long split it)
"""

"""
Changelog
20180409 work with tab delimited 
			addedd images,shapes and url images
20180328 added link with adocs . (to be better verified)
20180327 added image support for graphml
20180323 impoved yed export
20180322 added graphml basic yed export
20170927 Migrated to networkx 2.0
20170927 added json export 
"""

import os
import sys
import argparse

from bs4 import BeautifulSoup
import re
import networkx as nx
import configparser


# Add lib folder
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)),'lib'))
import mcv_lib
import mcv_graphml # graphml conversion
import mcv_vue # vue conversion



def exec_cmd(command_line):
	import subprocess
	import shlex
	# esegue un comando
	a_cmd = shlex.split(command_line)
	emit(command_line,1)
	subprocess.call(a_cmd) 	

def emit(line,level=0):
	"""
	print string based on verbosity
	if verbosity < 0 exit program
	"""
	
	if level < 0:
		sys.exit(line)
	elif level <= args.verbose :
		print(line)

def adoc2networkx(input_file):
	"""
	Read an adoc file and create a graph
	
	convert from adoc to docbook with the externa command and
	then parse it with beautifulsoap 
	"""

	docbook_xml = mcv_lib.adoc2docbook(input_file)
	return docbook2networkx_str(docbook_xml)
		
def docbook2networkx(input_file):
	docbook_xml=open(input_file)
	return docbook2networkx_str(docbook_xml)
		
def docbook2networkx_str(docbook_xml):
	
	G = nx.Graph()
	id_list = []	
	arch_label = ''

	from bs4 import BeautifulSoup
	soup = BeautifulSoup(docbook_xml,"xml")
		
	# main title of the document
	for tag in soup.findAll("info"):
		nodelabel = str(tag.title.string)
		mcv_prop={
				   'shape':'ellipse',
				   'font':'Arial-plain-24',
				   'fillcolor':'cadetblue',
				}		
	G.add_node(0,label=nodelabel,mc=mcv_prop)
		
	# all titles
	for tag in soup.findAll('section'):
		nodelabel = str(tag.title.string)
		try: 
			image= os.path.join(os.path.dirname(args.input),str(tag.imagedata['fileref']))
		except:
			image=''
		
		mcv_prop={
			'shape':'box',
			'image': image
		}
		id = tag['xml:id']
		try: 
			parent_id = tag.parent['xml:id']
		except:
			parent_id = 0 
		
		G.add_node(id,label=nodelabel,mc=mcv_prop)
		arch_label = ''
		G.add_edge(id,parent_id,label=arch_label)
		for desc in tag.children:
			if desc.name == 'simpara':
				nodelabel = str(desc.string)
				mcv_prop={	}
				G.add_node(nodelabel,label=nodelabel,mc=mcv_prop)
				arch_label = ''
				#parent_id = desc.parent.section['xml:id']
				parent_id = id
				G.add_edge(nodelabel,parent_id,label=arch_label)

			if desc.name == 'itemizedlist':
		
		
		### TODO NON FUNZIONA BENE il parsing del . (Label)
				try:
				# if desc.title:
					arch_label = desc.title.string
					G[id][parent_id]['label']=arch_label
				except: 
					arch_label = '' 
				
				for desc_item in desc.children:
					if desc_item.name == 'listitem':
						nodelabel = str(desc_item.simpara.string)
						mcv_prop={	}
						G.add_node(nodelabel,label=nodelabel,mc=mcv_prop)
						arch_label = ''
						#parent_id = desc.parent.section['xml:id']
						parent_id = id
						G.add_edge(nodelabel,parent_id,label=arch_label)
	
	# reconvert nodes sterting from 10 because otherwise conflict with ID in PathwayList for VUE
	G = nx.convert_node_labels_to_integers(G,10)
	
	return G

		

def tab2networkx(input_file):

	import csv
	""" 
	Convert a tab ident file e.g.
	
	The Cretans
	The City-States
		Knossos
		Festus
		Mallia
	What did they do?
		they produced
			fortified wine
			tasty oil
		Excellent sailors
	"""
	
	G = nx.Graph()
	nodes = []
	id_node = 0
	
	
	# Read tab indented file
	with open(input_file, 'r') as csvfile:
		reader = csv.reader(csvfile, dialect=csv.excel_tab)
		for row in reader:
			# Count the number of indents.
			indent = 0
			for i in row:
				if i == '':
					indent = indent+1
			try: 
				name = row[-1]
			except: 
				name = ""
			
			# Ignore comments
			if name and name[0] != '#':
				nodes.append([name, indent,id_node])
				id_node += 1
	
	# take all nodes and consider parents
	weightNo = len(nodes)
	for i in range(len(nodes)):
	
		# Look backwards and find the parent node (indent-1) 
		parent = ""
		for k in range(i):
			if (nodes[i][1]) -1 == nodes[k][1]:
				parent = nodes[k]
		#print(nodes[i],parent)
		
		node_row = nodes[i][0]
		node_row_list=node_row.split('|')
		node_label = node_row_list[0]
		node_mc = {}
		for node_element in node_row_list:
			if node_element.startswith('img::http'):
				# is a http link
				node_mc['image'] = node_element[5:]
			elif node_element.startswith('img::'):
				# is a local resource
				node_mc['image'] = os.path.join(os.path.dirname(args.input),node_element[5:])
			elif node_element.startswith('shape::'):
				node_mc['shape'] = node_element[7:]
			elif node_element.startswith('img_size::'):
				node_mc['img_size'] = node_element[10:]
			
		G.add_node(nodes[i][2],label=node_label,mc=node_mc)
		if parent != '':
			G.add_edge(nodes[i][2], parent[2])
	
	# reconvert nodes sterting from 10 because otherwise conflict with ID in PathwayList
	G = nx.convert_node_labels_to_integers(G,10)	
	return G
	
def csv2networkx(input_file):
	"""
	Read a csv file with format:
	id,label
	1,Label1
	1.1,Label2
	
	"""
	
	import csv
	G = nx.Graph()
	
	with open(input_file) as csvfile:
		reader = csv.DictReader(csvfile)

		
		# add all nodes
		for row in reader:
			# if ID has underscore peplace with standard .
			id = row['id'].replace("_",".")
			
			G.add_node(id,label=row["label"],mc={})
		
			id_split = row['id'].rpartition(".")
			
			if id_split[0] != '':
				G.add_edge(id, id_split[0])

	# reconvert nodes starting from 10 because otherwise conflict with ID in PathwayList
	G = nx.convert_node_labels_to_integers(G,10)	
	return G

def dot2networkx(input_file):
	"""read an dot file (grapviz) and return a networkx object
	"""

	G = nx.nx_agraph.read_dot(input_file)
	
	# add label if they don't have it
	for n in G:
		try:
			G.nodes[n]['label']
		except KeyError:
			# G.nodes[n]['label'] NOT defined
			G.nodes[n]['label'] = n
			
	# reconvert nodes sterting from 10 because otherwise conflict with ID in PathwayList
	G = nx.convert_node_labels_to_integers(G,10)		
	
	## add vue propierties
	for n in G:
		G.nodes[n]['mc'] = {}
		
		
		# STROKECOLOR  (Border color)
		if (G.nodes[n].get('color') != None):
			G.nodes[n]['mc']['strokecolor']=mcv_lib.convert_color(G.nodes[n].get('color'))
				
		if (G.nodes[n].get('style') != None) and (G.nodes[n].get('style').find('fill')>=0) and (G.nodes[n].get('fillcolor') == None):
			# this graph has style 'filled', but it has no fillcolor (which is used as the background color)
			if (G.nodes[n].get('color') != None):     # if the graph has a color, it is used as the background color
				G.nodes[n]['mc']['fillcolor'] = G.nodes[n]['color']
			elif (G.nodes[n].get('bgcolor') != None):   # if the graph has a bgcolor, it is used as the background color
				G.nodes[n]['mc']['fillcolor'] = G.nodes[n]['bgcolor']
			else:
				G.nodes[n]['mc']['fillcolor'] = config['DEFAULT']['NODE_FILLCOLOR']
		else:
			if G.nodes[n].get('fillcolor') != None:
				G.nodes[n]['mc']['fillcolor'] = G.nodes[n]['fillcolor']
	
		# STROKECOLOR
		if (G.nodes[n].get('shape') != None):
			G.nodes[n]['mc']['shape']=G.nodes[n]['shape']
		
	return G

def vue2networkx(inputfile):
	"""
	Read a .vue file and generate a graph
	"""
	file = open(inputfile,"r")
	xml = file.read()
	xml = re.sub("<child ID", "<child identity", xml, 0, re.I | re.M | re.DOTALL)
	soup = BeautifulSoup(xml,"lxml")
	
	G = nx.Graph()


	for item in soup.findAll("child"):

		if item['xsi:type'] == "node":
			try:
				nodelabel = item['label']
			except: 
				nodelabel = ''
			#nodelabel = re.sub("\n", " ", nodelabel, 0, re.I | re.M | re.DOTALL)
			#nodelabel = re.sub("\ \ ", " ", nodelabel, 0, re.I | re.M | re.DOTALL)

			G.add_node(
					item['identity'],label=nodelabel,mc={
						'x':float(item['x']),
						'y':float(item['y']),
						'width':float(item['width']),
						'height':float(item['height']),
						'fillcolor':str(item.fillcolor.string),			# Background color of the node in html notation
						'strokecolor':str(item.strokecolor.string),		# Border color of the node
						'textcolor':str(item.textcolor.string),			# Color of the text
						'font':str(item.font.string),
						'shape':str(item.shape['xsi:type']),
						}
				)

		
		# I add all the edges
		if item['xsi:type'] == "link":
		
					
			try:
				G.add_edge(item.id1.string, item.id2.string,label=item['label'])
			except:
				G.add_edge(item.id1.string, item.id2.string)

	# reconvert nodes sterting from 10 because otherwise conflict with ID in PathwayList
	G = nx.convert_node_labels_to_integers(G,10)	
	
	return G

def networkx2json(G,outfile):
	
	#  Object of type 'Graph' is not JSON serializable
	#
	import json
	from networkx.readwrite import json_graph
	data1 = json_graph.node_link_data(G)
	with open(outfile+'.json', 'w') as f:
		json.dump(data1, f,indent=4)

def networkx2gml(G,outfile):
	"""
	write the Graph in the gml format
	"""
	
	H = G.copy()
	
	for n in H:
	
		# add vue attributes if presents
		if H.nodes[n].get('label') != None:		
			H.nodes[n]['LabelGraphics']= {'text':H.nodes[n]['label']}
		
		H.nodes[n]['graphics'] = {}
		
		if H.nodes[n]['mc'].get('x') != None:		
			H.nodes[n]['graphics']['x']= H.nodes[n]['mc']['x']

		if H.nodes[n]['mc'].get('y') != None:		
			H.nodes[n]['graphics']['y']= H.nodes[n]['mc']['y']			
			
		if H.nodes[n]['mc'].get('width') != None:		
			H.nodes[n]['graphics']['w']= H.nodes[n]['mc']['width']
	
		if H.nodes[n]['mc'].get('x') != None:		
			H.nodes[n]['graphics']['height']= H.nodes[n]['mc']['height']
		
		
		# Remove internal mindconv 'mc' attributes	
		try: 
			a = 1
			del H.nodes[n]['mc']
		except:
			pass

	nx.write_gml(H,outfile+'.gml')
		

def networkx2dot(G,outfile):
	"""
	write the Graph in the dot format
	"""	
	# and the following code block is not needed
	# but we want to see which module is used and
	# if and why it fails
	try:
		import pygraphviz
		from networkx.drawing.nx_agraph import write_dot
		#using package pygraphviz
	except ImportError:
		try:
			import pydotplus
			from networkx.drawing.nx_pydot import write_dot
			#using package pydotplus
		except ImportError:
			print("Both pygraphviz and pydotplus were not found ")
			print("see http://networkx.github.io/documentation"
			  "/latest/reference/drawing.html for info")
			print()
		raise

	H = G.copy()
	for n in H:
		
		# add mc attributes if presents
		if H.nodes[n]['mc'].get('shape') != None:		
			H.nodes[n]['shape']= mcv_lib.convert_shape(H.nodes[n]['mc']['shape'],'dot')
		
		# Remove internal mindconv 'mc' attributes	
		try: 
			del H.nodes[n]['mc']
		except:
			pass

	write_dot(H,outfile+'.gv')
	#print("dot -Tpng -oou#00000000t.png "+outfile+'.gv')	

def networkx2mm(G,outfile):
	"""
	convert the networkx graph to mm (freemind o freeplane) format
	TODO: use beautiful soup instead of sting concatenation
	"""
	
	
	out = mm_xml_head()

	# analisys of all nodes
	for n in G:
		out += mm_write_node(n,G.nodes[n])

	# analisys of all edges 
	id =  0
	for (e_from,e_to,e_data) in G.edges(data=True):
		out += mm_write_link(id,e_from,e_to,e_data)
		id += 1
	
	out += mm_xml_tail()
	
	f = open(outfile+'.mm','w')
	f.write(out)	




	
def mm_xml_head():
	out="""
	TODO_mm_xml_head
	"""
	return out
	
def mm_xml_tail():
	out="""
	TODO_mm_xml_tail
	"""
	return out

def mm_write_node(n,node):
	return "TODO_NODE"

def mm_write_link(id,e_from,e_to,e_data):
	return "TODO_link"

	
def split_in_multiple_line(string):
	out =re.sub(" ","\n", string)
	return out
	

def mc_process_graph(G):
	"""process graph (i.e. split lablel in several lines, ... ) """
	

	for n in G:
		try:
			node_label = G.nodes[n]['label']
			if config['DEFAULT']['SPLIT_NODE_LABEL'].lower()=='yes' or config['DEFAULT']['SPLIT_NODE_LABEL'].lower()=='true':
				G.nodes[n]['label'] = split_in_multiple_line(node_label)
		except:
			pass
	
	for (e_from,e_to,e_data) in G.edges(data=True):
		try:
			edge_label = e_data['label']
		except:
			pass

def show_networkx(G):
	"""
	print G  (used for debugging purpose)
	"""
	print(nx.info(G))
	print('--- NODES ---')
	for n in G:
		print(n,G.nodes[n])
	print('--- EDGES ---')
	for (e_from,e_to,e_data) in G.edges(data=True):
		print(e_from,e_to,e_data)

def run():
	
	
	# read configuration from ini
	global config
	config = configparser.ConfigParser()
	if args.configuration_file == '':
		script_path = os.path.dirname(os.path.realpath(__file__))
		config_file = os.path.join(script_path,'mindconv-config.ini')
	else:
		config_file = args.configuration_file
		
	config.read(config_file)
	
	input_format = args.input_format
	if args.input_format == 'auto':
		input_format = os.path.splitext(args.input)[1][1:]
	if input_format == 'vue':
		G = vue2networkx(args.input)
	elif input_format == 'dot' or input_format == 'gv':
		G = dot2networkx(args.input)
	elif input_format == 'csv':
		G = csv2networkx(args.input)
	elif input_format == 'adoc':
		G = adoc2networkx(args.input)
	elif input_format in ['docbook','xml']:
		G = docbook2networkx(args.input)
	elif input_format == 'txt':
		G = tab2networkx(args.input)
	else:
		print("no input format found")
		exit(-1)
	
	
	if args.verbose:
		show_networkx(G)

	# process graph (i.e. split label in several lines, ... ) 
	mc_process_graph(G)
	
	#write networkx graph to file
	if args.output_format=='gml' or args.output_format=='all':
		networkx2gml(G,args.output)
	
	if args.output_format=='graphml' or args.output_format=='all':
		mcv_graphml.config = config
		#mcv_graphml.networkx2graphml(G,args.output)
		mcv_graphml.networkx2graphml_yed(G,args.output)
	
	if args.output_format in ['dot','svg','png','jpg','all']:
		networkx2dot(G,args.output)
		if args.output_format in ['png','all']:
			comando = "dot -Tpng -o{}.png {}.gv".format(args.output,args.output)
			exec_cmd(comando)
		if args.output_format in ['svg','all']:
			comando = "dot -Tsvg -o{}.svg {}.gv".format(args.output,args.output)
			exec_cmd(comando)
		if args.output_format in ['jpg','all']:
			comando = "dot -Tjpg -o{}.jpg {}.gv".format(args.output,args.output)
			exec_cmd(comando)

	if args.output_format in ['vue','all']:
		mcv_vue.config = config
		mcv_vue.networkx2vue(G,args.output)

	if args.output_format in ['vue','all']:
		networkx2json(G,args.output)
		
# TODO
#	if args.output_format=='mm' or args.output_format=='all':
#		networkx2mm(G,args.output)

def main():
	
	global args	
	parser = argparse.ArgumentParser(description=program_description)
	parser.add_argument('input', help='Input file name')
	parser.add_argument('-v', '--verbose', action='count', default=0)
	parser.add_argument('-o','--output',help='Output file name', default='out')
	parser.add_argument('-f',"--output-format" , help="Output Format",choices=['gml','graphml','dot','vue','json','png','svg','jpg','all'],default='all')
	parser.add_argument("--input-format" , help="Input Format",choices=['dot','gv','vue','csv','auto'],default='auto')
	parser.add_argument('-c',"--configuration-file" , help="Configuration file",default='')
	

	args = parser.parse_args()

	run()

	
	
if __name__ == '__main__':
	main()
	


